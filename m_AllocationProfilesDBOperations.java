package com.gma.localbrokeringserver.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.gma.allocationprofile.clearingaccounts.ITradeAllocationProfile;
import com.gma.allocationprofile.clearingaccounts.TradeAllocationProfile;
import com.gma.allocationprofile.clearingaccounts.TradeAllocationRule;
import com.gma.dbaccess.DBContextInterface;
import com.gma.dbaccess.DBException;
import com.gma.dbaccess.DBManager;
import com.gma.dbaccess.DBUtilsInterface;
import com.gma.dbaccess.TransactionalContext;
import com.gma.dbaccess.TransactionalContextInterface;
import com.gma.localbrokeringserver.BrokerAccountsManagerNEW;
import com.gma.log.AppLogger;
import com.gma.managementserver.ManagementServersManager;
import com.gma.trading.broker.ClearingBrokerAccount;
import com.gma.user.User;
import com.gma.util.date.DateConverter;

// MongoDB imports
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.MongoException;

/**
 * Object responsible with the persistence of the allocation profiles.
 * @author csandru
 *
 */
public class m_AllocationProfilesDBOperations {
	Logger logger = AppLogger.getLogger(AppLogger.LOGGER_NAME_BROKERING_SERVER);
	
	private static final String ALLOCATION_PROFILE_TABLE_NAME = "allocation_profile";
	private static final String ALLOCATION_PROFILE_DETAIL_TABLE_NAME = "allocation_profile_detail";
	private static final String ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME = "allocated_accounts_on_account";
	
	private static final String AP_PROFILE_ID = "allocation_profile_id";
	private static final String AP_MODIFICATION_ROOT_ID = "modification_root_id";
	private static final String AP_OWNER_ID = "owner_id";
	private static final String AP_PROFILE_NAME = "name";
	private static final String AP_IS_DEPRECATED = "is_deprecated";
	private static final String AP_DATE_CREATED = "date_created";
	private static final String AP_DATE_MODIFIED = "date_modified";
	private static final String AP_IS_LAST = "is_last";
	private static final String AP_IS_DELETED = "is_deleted";

	private static final String APD_PROFILE_ID = "profile_id";
	private static final String APD_CLEARING_ACCOUNT_ID = "clearing_account_id";
	private static final String APD_PERCENTAGE = "percentage";

	private static final String AAA_CLEARING_ACCOUNT_ID = "clearing_account_id";
	private static final String AAA_ALLOCATED_CLEARING_ACCOUNT_ID = "allocated_clearing_account_id";
	private static final String AAA_DATE_CREATED = "date_created";
	
	private static final String YES_VALUE = "YES";

	private static final String NO_VALUE = "NO";

	private DBUtilsInterface utils = DBManager.getInstance().getDBUtils();
	
	/**
	 * The Broker Accounts Manager
	 */
	private BrokerAccountsManagerNEW bam;

	public m_AllocationProfilesDBOperations(BrokerAccountsManagerNEW bam) {
		super();
		this.bam = bam;
	}
	
	public void addAllocationProfile(ITradeAllocationProfile allocationProfile)
			throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			addAllocationProfile(tc, allocationProfile);
			if (tc.isExceptionEncountered()){
				tc.rollbackTransaction();
			}
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	
	public void saveAllocationProfile(ITradeAllocationProfile allocationProfile)
			throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			saveAllocationProfile(tc, allocationProfile);
			if (tc.isExceptionEncountered()){
				tc.rollbackTransaction();
			}
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	
	public void deleteAllocationProfile(String profileId)
			throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			deleteAllocationProfile(tc, profileId);
			if (tc.isExceptionEncountered()){
				tc.rollbackTransaction();
			}
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	
	public void setAllocationProfileDeprecated(String profileId)
			throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			setAllocationProfileDeprecated(tc, profileId);
			if (tc.isExceptionEncountered()){
				tc.rollbackTransaction();
			}
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
		
	public void setTradeAllocatedAccounts(ClearingBrokerAccount account,
			List<ClearingBrokerAccount> allocatedAccounts)
					throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			setTradeAllocatedAccounts(tc, account, allocatedAccounts);
			if (tc.isExceptionEncountered()){
				tc.rollbackTransaction();
			}
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	
	public void addAllocationProfile(TransactionalContextInterface tc, ITradeAllocationProfile allocationProfile)
			throws /*DBException*/ MongoException{
		long dateCreated = DateConverter.getCurrentTime();
            
      DB             db = null;
      BasicDBObject doc = null;
      DBCollection coll = null;
            
		try {
			String strDeprecated;
			if (allocationProfile.isDeprecated()){
				strDeprecated = "'" + YES_VALUE + "'";
			}else{
				strDeprecated = "'" + NO_VALUE + "'";
			}
			
			String strLast;
			if (allocationProfile.isLast()){
				strLast = "'" + YES_VALUE + "'";
			}else{
				strLast = "'" + NO_VALUE + "'";
			}
			
//			String queryINSERT = "INSERT INTO "
//					+ ALLOCATION_PROFILE_TABLE_NAME
//					+ utils.tableColumnsList(AP_PROFILE_ID, AP_MODIFICATION_ROOT_ID, AP_PROFILE_NAME, AP_OWNER_ID,
//							AP_IS_DEPRECATED, AP_IS_LAST, AP_DATE_CREATED, AP_DATE_MODIFIED)
//					+ " VALUES "
//					+ utils.tableValuesList(
//							allocationProfile.getId(),
//							allocationProfile.getModificationRootId(),
//							utilsEscape(allocationProfile.getName()),
//							"" + allocationProfile.getOwner().getId(),
//							strDeprecated,
//							strLast,
//							"" + dateCreated,
//							"" + dateCreated);

                  /////////////////////////////////////////////////////////////////////////////////////////////////
                  // mongo query, m = mongo connection, coll = collection, q = query, db = the database
                  doc = new BasicDBObject(AP_PROFILE_ID, allocationProfile.getId()).
                                   append(AP_MODIFICATION_ROOT_ID, allocationProfile.getModificationRootId()).
                                   append(AP_PROFILE_NAME,utilsEscape(allocationProfile.getName()). 
                                   append(AP_OWNER_ID, allocationProfile.getOwner().getId()).
                                   append(AP_IS_DEPRECATED, strDeprecated).
                                   append(AP_IS_LAST, strLast).
                                   append(AP_DATE_CREATED, dateCreated).
                                   append(AP_DATE_MODIFIED, dateCreated));    // shouldn't this be 'dateModified'? BR
                  
                  coll = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
                  coll.insert(doc);
                  doc.clear();
                  //////////////////////////////////////////////////////////////////////////////////////////////////
                  
//			tc.execute(queryINSERT);
//			addAllocationProfileDetails(tc, allocationProfile);
		} catch (/*DBException*/ MongoException me) {
			logger.severe(AppLogger.buildExceptionString(me));
//			tc.exceptionEncountered(e);
			throw me;         // ??? BR
		}
		
	}

	private void addAllocationProfileDetails(TransactionalContextInterface tc,
			ITradeAllocationProfile allocationProfile) throws /*DBException*/ MongoException {
		String queryINSERT;
		//add profile details
		List<TradeAllocationRule> rules = allocationProfile.getAllocationRules();
            
      // mongo query, m = mongo connection, coll = collection, q = query, db = the database
      DB             db = null;
      BasicDBObject doc = null;
      DBCollection coll = null;
            
		for (TradeAllocationRule rule : rules){
//			queryINSERT = "INSERT INTO "
//					+ ALLOCATION_PROFILE_DETAIL_TABLE_NAME
//					+ utils.tableColumnsList(APD_PROFILE_ID, APD_CLEARING_ACCOUNT_ID, 
//							APD_PERCENTAGE)
//					+ " VALUES "
//					+ utils.tableValuesList(
//							allocationProfile.getId(),
//							"" + rule.getEntity().getInternalId(),
//							"" + rule.getAllocatedPercentage()
//							);
//			tc.execute(queryINSERT);
               
               ////////////////////////////////////////////////////////////////
               try{
                  doc = new BasicDBObject(APD_PROFILE_ID, allocationProfile.getId()).
                                   append(APD_CLEARING_ACCOUNT_ID, rule.getEntity().getInternalId()).
                                   append(APD_PERCENTAGE, rule.getAllocatedPercentage());
                  
                  coll = db.getCollection(ALLOCATION_PROFILE_DETAIL_TABLE_NAME);
                  coll.insert(doc);
                  doc.clear();
               } catch(MongoException me){
                  // do somethig with the exception
               }
               ////////////////////////////////////////////////////////////////
		}
	}
	
	/**
	 * Save an allocation profile by completely replacing all its details
	 * @param tc
	 * @param allocationProfile
	 * @throws DBException
	 */
	public void saveAllocationProfile(TransactionalContextInterface tc, ITradeAllocationProfile allocationProfile)
			throws /*DBException*/ MongoException{
      
      long dateModified = DateConverter.getCurrentTime();
      
      try {
         
//			String queryUPDATE = "UPDATE "
//				+ ALLOCATION_PROFILE_TABLE_NAME
//				+ " SET "
//				+ utils.tableUpdateList(
//						AP_PROFILE_NAME, 
//						utilsEscape(allocationProfile.getName()),						
//						AP_MODIFICATION_ROOT_ID, 
//						utilsEscape(allocationProfile.getModificationRootId()),
//						AP_OWNER_ID,
//						"" + allocationProfile.getOwner().getId(),						
//						AP_IS_DEPRECATED, 
//						"'" + (allocationProfile.isDeprecated() ? YES_VALUE : NO_VALUE) + "'",
//						AP_IS_LAST, 
//						"'" + (allocationProfile.isLast() ? YES_VALUE : NO_VALUE) + "'",
//						AP_DATE_MODIFIED,
//						"" + dateModified)
//						 + " WHERE " + AP_PROFILE_ID + "="
//				+ allocationProfile.getId();
//
//			tc.execute(queryUPDATE);
                  
         ///////////////////////////////////////////////////////////////////////////
         // mongo query, m = mongo connection, coll = collection, q = query, db = the database
         DB               db = null;
         BasicDBObject   doc = null;
         BasicDBObject   qry = null;
         BasicDBObject   rpl = null;
         DBCollection   coll = null;

         // update (where clause, replacement object)
         qry = new BasicDBObject("AP_PROFILE_ID", allocationProfile.getId());
         
         // note the embedded query here
         doc = new BasicDBObject(AP_PROFILE_NAME, allocationProfile.getName().
                          append(AP_MODIFICATION_ROOT_ID, allocationProfile.getModificationRootId()).
                          append(AP_OWNER_ID, allocationProfile.getOwner().getId()).
                          append(AP_IS_DEPRECATED, 
                                (allocationProfile.isDeprecated() ? YES_VALUE : NO_VALUE)));
         
         // don't forget to use $set with update ops, this is a good, fairly complicated example
         rpl = new BasicDBObject("$set", doc);
         
         coll = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
         
         coll.update(qry, rpl);
         
         ///////////////////////////////////////////////////////////////////////////
                  
			//delete the existing details
			deleteAllocationProfileDetails(tc, allocationProfile.getId());
					
			//add the new profile details
			addAllocationProfileDetails(tc, allocationProfile);
         
		} catch (/*DBException*/ MongoException me) {
			logger.severe("Error updating the allocation profile " + allocationProfile.toString()
					+ ": "
					+ AppLogger.buildExceptionString(me));
			//tc.exceptionEncountered(me);
			throw me;  // ? weird place to throw? BR
		}
	}

		
	private void deleteAllocationProfileDetails(TransactionalContextInterface tc,
                                               String profileId) throws DBException {
      
		//delete the profile details
//		String deleteQuery = "DELETE FROM "
//				+ ALLOCATION_PROFILE_DETAIL_TABLE_NAME
//				+ " WHERE "					
//				+ APD_PROFILE_ID + "=" + profileId; 
//		tc.execute(deleteQuery);
      
      //////////////////////////////////////////////////////////////////////////////////////
      // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
      DB               db = null;
      BasicDBObject   doc = null;
      BasicDBObject   rpl = null;
      BasicDBObject   qry = null;
      DBCollection   coll = null;
      
      qry = new BasicDBObject(AP_PROFILE_ID, profileId);
      
      coll = db.getCollection(ALLOCATION_PROFILE_DETAIL_TABLE_NAME);
      coll.remove(qry);
         
	}
 	
	public void deleteAllocationProfile(TransactionalContextInterface tc, String profileId)
			throws /*DBException*/ MongoException{
		//only mark it as deleted
		long dateModified = DateConverter.getCurrentTime();
      
		try{
//			String deleteQuery = "UPDATE "
//					+ ALLOCATION_PROFILE_TABLE_NAME
//					+ " SET " + AP_IS_DELETED + "=" + "'" + YES_VALUE + "'"
//					+ ", " + AP_DATE_MODIFIED + "=" + dateModified
//					+ " WHERE "					
//					+ AP_PROFILE_ID + "=" + profileId; 
         
         //////////////////////////////////////////////////////////////////////////////////////
         // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
         DB               db = null;
         BasicDBObject   doc = null;
         BasicDBObject   rpl = null;
         BasicDBObject   qry = null;
         DBCollection   coll = null;
         
         // update query
         qry = new BasicDBObject(AP_PROFILE_ID, profileId);
         doc = new BasicDBObject(AP_IS_DELETED, YES_VALUE).append(AP_DATE_MODIFIED, dateModified);
         
         rpl = new BasicDBObject("$set", doc);
         
         coll = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
         
         coll.update(qry, rpl);
         
//			//tc.execute(deleteQuery);
			deleteAllocationProfileDetails(tc, profileId);
		} catch (/*DBException*/ MongoException me) {
			logger.severe("Error deleting the allocation profile " + profileId
					+ ": "
					+ AppLogger.buildExceptionString(me));
			tc.exceptionEncountered(me);
			throw me;
		}
	}


	public void setAllocationProfileDeprecated(TransactionalContextInterface tc, String profileId)
	throws /*DBException*/ MongoException {
		long dateModified = DateConverter.getCurrentTime();
      
		try {
//			String queryUPDATE = "UPDATE "
//				+ ALLOCATION_PROFILE_TABLE_NAME
//				+ " SET "
//				+ utils.tableUpdateList(												
//						AP_IS_DEPRECATED, 
//						"'" + YES_VALUE + "'",
//						AP_IS_LAST, 
//						"'" + NO_VALUE + "'",
//						AP_DATE_MODIFIED,
//						"" + dateModified)
//						 + " WHERE " + AP_PROFILE_ID + "=" + profileId;
//
//			tc.execute(queryUPDATE);	
         //////////////////////////////////////////////////////////////////////////////////////
         // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
         DB               db = null;
         BasicDBObject   doc = null;
         BasicDBObject   rpl = null;
         BasicDBObject   qry = null;
         DBCollection   coll = null;
         
         coll = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
         qry = new BasicDBObject(AP_PROFILE_ID, profileId);
         doc = new BasicDBObject(AP_IS_DEPRECATED, YES_VALUE).append(AP_IS_LAST, NO_VALUE).
                          append(AP_DATE_MODIFIED, dateModified);
         rpl = new BasicDBObject("$set", doc);
         
         coll.update(qry, rpl);
         
		} catch (/*DBException*/ MongoException me) {
			logger.severe("Error deprecating the allocation profile " + profileId
					+ ": "
					+ AppLogger.buildExceptionString(me));
			tc.exceptionEncountered(me);
			throw me;
		}
	}
//HERE/////////////////	
	public List<ITradeAllocationProfile> readAllocationProfiles()
			throws DBException{
		List<ITradeAllocationProfile> result = new ArrayList<ITradeAllocationProfile>();
		
		DBContextInterface dbContext = null;
      
      //////////////////////////////////////////////////////////////////////////////////////
      // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
      DB                 db = null;
      BasicDBObject     doc = null;
      BasicDBObject     rpl = null;
      BasicDBObject     qry = null;
      DBCollection     coll = null;
      DBCollection  temp_c1 = null;
      DBCollection  temp_c2 = null;
      DBCursor          csr = null;
      
		try {
			//ordering is important for the 
//			String sqlStr = "SELECT * FROM "
//					+ ALLOCATION_PROFILE_TABLE_NAME
//					+ " WHERE " + AP_IS_DELETED + "<>'" + YES_VALUE + "'"
//					+ " ORDER BY " + AP_PROFILE_ID + " ASC";
//			dbContext = DBManager.getInstance().getDBContext();
//
//			List<Map<String, Object>> allocationProfiles = dbContext
//					.getAllRowsFromQuery(sqlStr);
         
         qry = new BasicDBObject(AP_IS_DELETED, new BasicDBObject("$ne", YES_VALUE));        
         coll = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
         csr  = coll.find(qry).sort(new BasicDBObject(AP_PROFILE_ID, 1)); 
         
/////////////////////////////////////////////// NASTY! ///////////////////////////////////////////////
//       sqlStr = "SELECT " + ALLOCATION_PROFILE_DETAIL_TABLE_NAME + ".* FROM "
//					+ ALLOCATION_PROFILE_TABLE_NAME + "," + ALLOCATION_PROFILE_DETAIL_TABLE_NAME
//					+ " WHERE " + AP_IS_DELETED + "<>'" + YES_VALUE + "'"
//					+ " AND " + ALLOCATION_PROFILE_DETAIL_TABLE_NAME + "." + APD_PROFILE_ID + "="
//								+ ALLOCATION_PROFILE_TABLE_NAME + "." + AP_PROFILE_ID
//					+ " ORDER BY " + ALLOCATION_PROFILE_TABLE_NAME + "." + AP_PROFILE_ID + " ASC";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////         
// implicit join here in where clause
//
// Select all allocation_profile_detail_table_name column names 
// from   allocation_profile_table_name and 
//        allocation_profile_detail_table_name 
// where (ap_is_deleted != yes_value && allocation_profile_detail_table_name.apd_profile_id == 
//       allocation_profile_table_name.ap_profile_id
// order result by allocation_profile_table_name.ap_profile_id ascending
         
         temp_c1 = db.getCollection(ALLOCATION_PROFILE_TABLE_NAME);
         temp_c2 = db.getCollection(ALLOCATION_PROFILE_DETAIL_TABLE_NAME);
         
//  MongoDB Get names of all keys in collection (Javascript version)
//mr = db.runCommand({ 
// "mapreduce" : "things", 
// "map" : function() {
//   for (var key in this) { 
//         emit(key, null); } },
// "reduce" : function(key, stuff) { 
//  return null; }, "out": "things" + "_keys" })
// Then run distinct on the resulting collection so as to find all the keys:
//    db[mr.result].distinct("_id") ["foo", "bar", "baz", "_id", ...]          
                  
         
			///////// needs to modified for Mongo /////////
               List<Map<String, Object>> allocationProfilesDetails = dbContext
					.getAllRowsFromQuery(sqlStr);
			
               int detailsIndex = 0;
               int allocationDetailsSize = allocationProfilesDetails.size();
               int len = allocationProfiles.size();

               for (int i = 0; i < len; i++) {
                     Map<String, Object> allocationProfile = allocationProfiles.get(i);
                     TradeAllocationProfile tap = new TradeAllocationProfile(allocationProfile.get(AP_PROFILE_ID).toString());
                     tap.setModificationRootId(allocationProfile.get(AP_MODIFICATION_ROOT_ID).toString());
                     tap.setName(allocationProfile.get(AP_PROFILE_NAME).toString());
                     User user = ManagementServersManager.getInstance().getDefaultServer().getUser(null, Integer.parseInt(allocationProfile.get(AP_OWNER_ID).toString()));
                     if (user == null){
                           continue;
                     }
                     tap.setOwner(user);
                     tap.setDeprecated(allocationProfile.get(AP_IS_DEPRECATED).toString().equals(YES_VALUE) ? true : false);
                     tap.setLast(allocationProfile.get(AP_IS_LAST).toString().equals(YES_VALUE) ? true : false);
                     result.add(tap);
                     //add the details in the profile: assumed that all the relevant details are coming together because of the sorting above
                     while(detailsIndex < allocationDetailsSize){
                           Map<String, Object> allocationProfileDetails = allocationProfilesDetails.get(detailsIndex);
                           String profileId = allocationProfileDetails.get(APD_PROFILE_ID).toString();
                           if (profileId.equals(tap.getId())){
                                 detailsIndex++;
                                 String clearingAccountId = allocationProfileDetails.get(APD_CLEARING_ACCOUNT_ID).toString();						
                                 ClearingBrokerAccount account = bam.getClearingBrokerAccount(Long.parseLong(clearingAccountId));
                                 if (bam == null){
                                       continue;
                                 }
                                 String percentageStr = allocationProfileDetails.get(APD_PERCENTAGE).toString();
                                 TradeAllocationRule tar = new TradeAllocationRule(account, Double.parseDouble(percentageStr));
                                 tap.addAllocationRule(tar);
                           }else{
                                 break;
                           }
                     }

               }
		}catch(DBException e){
			logger.severe("Error reading allocations for account: " 
					+ ": "
					+ AppLogger.buildExceptionString(e));
			throw e;
		} finally {
			if (dbContext != null) {
				dbContext.release();
			}
		}
		return result;
	}
	
	public void setTradeAllocatedAccounts(TransactionalContextInterface tc, ClearingBrokerAccount account,
			List<ClearingBrokerAccount> allocatedAccounts)
					throws /*DBException*/MongoException{
         //////////////////////////////////////////////////////////////////////////////////////
         // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
         DB               db = null;
         BasicDBObject   doc = null;
         BasicDBObject   qry = null;
         DBCollection   coll = null;
         
         try {
               //delete existing entries
//               String queryDELETE = "DELETE FROM "
//                     + ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME
//                     + " WHERE " + AAA_CLEARING_ACCOUNT_ID + "="
//                     + account.getInternalId();

               qry = new BasicDBObject(AAA_CLEARING_ACCOUNT_ID, account.getInternalId());
               
               coll = db.getCollection(ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME);
               coll.remove(qry);

               //tc.execute(queryDELETE);
               //add the new allocations
               long dateCreated = DateConverter.getCurrentTime();
               for (ClearingBrokerAccount allocatedAccount : allocatedAccounts){
//                     String queryINSERT = "INSERT INTO "
//                                 + ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME
//                                 + utils.tableColumnsList(AAA_CLEARING_ACCOUNT_ID, AAA_ALLOCATED_CLEARING_ACCOUNT_ID, AAA_DATE_CREATED)
//                                 + " VALUES "
//                                 + utils.tableValuesList(
//                                             "" + account.getInternalId(),
//                                             "" + allocatedAccount.getInternalId(),								
//                                             "" + dateCreated);
                   doc = new BasicDBObject(AAA_CLEARING_ACCOUNT_ID, account.getInternalId().
                                    append(AAA_ALLOCATED_CLEARING_ACCOUNT_ID, allocatedAccount.getInternalId().
                                    append(AAA_DATE_CREATED, dateCreated)));  
                  
                   coll = db.getCollection(ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME);
                   coll.insert(doc);
                   doc.clear();
                     //tc.execute(queryINSERT);
               }
         } catch (/*DBException*/ MongoException me) {
               logger.severe("Error setting allocations for account: " + account.toString()
                           + ": "
                           + AppLogger.buildExceptionString(me));
               tc.exceptionEncountered(me);
               throw me;
         }
	}
	
	public Map<ClearingBrokerAccount, List<ClearingBrokerAccount>> readTradeAllocatedAccounts()
			throws /*DBException*/ MongoException{
		Map<ClearingBrokerAccount, List<ClearingBrokerAccount>> result = new HashMap<>();
		DBContextInterface dbContext = null;
            
            //////////////////////////////////////////////////////////////////////////////////////
            // mongo query, m = mongo connection, coll = collection, q = query, db = the database 
            DB               db = null;
            DBCollection   coll = null;
            DBCursor        csr = null;
            
		try {
			//ordering is important for the algorithm below
//			String sqlStr = "SELECT * FROM " +
//					ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME +
//					" ORDER BY " + AAA_CLEARING_ACCOUNT_ID;
                  
                  coll = db.getCollection(ALLOCATED_ACCOUNTS_ON_ACCOUNT_TABLE_NAME);
                  csr  = coll.find().sort(new BasicDBObject(AAA_CLEARING_ACCOUNT_ID, 1));
                  
			dbContext = DBManager.getInstance().getDBContext();
			
			String currentClearingAccountId = null;
			List<ClearingBrokerAccount> currentlyAllocatedAccounts = null;
			List<Map<String, Object>> allocations = dbContext.getAllRowsFromQuery(sqlStr);
			for (Map<String, Object> allocation : allocations){
				String clearingAccountId = allocation.get(AAA_CLEARING_ACCOUNT_ID).toString();
				if (!clearingAccountId.equals(currentClearingAccountId)){
					//new account; retrieve it
					ClearingBrokerAccount account = bam.getClearingBrokerAccount(Long.parseLong(clearingAccountId));
					if (account == null){
						logger.severe("Account not found while reading trade allocations: " + clearingAccountId);
						continue;
					}
					currentClearingAccountId = clearingAccountId;
					//create a list for the account
					currentlyAllocatedAccounts = new ArrayList<>();					
					result.put(account, currentlyAllocatedAccounts);
				}
				//add the allocated account to the current list
				String allocatedClearingAccountId = allocation.get(AAA_ALLOCATED_CLEARING_ACCOUNT_ID).toString();
				ClearingBrokerAccount allocatedAccount = bam.getClearingBrokerAccount(Long.parseLong(allocatedClearingAccountId));
				if (allocatedAccount == null){
					logger.severe("Allocated account not found while reading trade allocations: " + allocatedClearingAccountId);
					continue;
				}
				currentlyAllocatedAccounts.add(allocatedAccount);
			}
				
		} catch (/*DBException*/ me) {
			logger.severe("Error reading accounts trade allocations"
					+ ": "
					+ AppLogger.buildExceptionString(me));
			throw me;
		} finally {
			if (dbContext != null) {
				dbContext.release();
			}
		}
		return result;
	}
	
	public String getNextAllocationProfileId()
			throws DBException{
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			return tc.getNextId(ALLOCATION_PROFILE_TABLE_NAME);
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	

	private String utilsEscape(String value) {
		return "'" + utils.escape(value) + "'";
	}

}
