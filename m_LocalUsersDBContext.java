/**
 * @(#)LocalUsersDBContext.java
 * @version: 1.11
 * 
 * Copyright (c) 2005-2008 Green Mountain Analytics 
 * This software is the confidential and proprietary information 
 * of Green Mountain Analytics. You shall not disclose such Confidential   
 * Information and shall use it only in accordance with the terms 
 * of the license agreement you entered into with Green Mountain Analytics.
 */

package com.gma.managementserver.local.users;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import com.gma.dbaccess.DBContextInterface;
import com.gma.dbaccess.DBException;
import com.gma.dbaccess.DBManager;
import com.gma.dbaccess.DBUtilsInterface;
import com.gma.dbaccess.TransactionalContext;
import com.gma.dbaccess.TransactionalContextInterface;
import com.gma.exchange.Exchange;
import com.gma.group.orderticketpattern.OrderTif;
import com.gma.group.orderticketpattern.OrderType;
import com.gma.log.AppLogger;
import com.gma.managementserver.local.ManagementConstants;
import com.gma.managementserver.local.PrivilegeUtils;
import com.gma.managementserver.naming.DuplicateNameException;
import com.gma.managementserver.users.UsersDBContext;
import com.gma.session.LoginContext;
import com.gma.strategytag.StrategyTagsSerDes;
import com.gma.symbol.SymbolType;
import com.gma.user.User;
import com.gma.user.UserDefaultOrder;
import com.gma.user.UserRights;
import com.gma.user.UserRole;
import com.gma.user.UserRoute;
import com.gma.util.StringTemplate;

import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;

/**
 * Executes all 'users' table and related table 'user_functions_rights'.
 * operations. <br>
 * TODO: optimize 'update' operation - now updates the relted tables by delete and insert operations.
 * @version 1.0
 * @author Alin Alexandru
 * @date Apr 19, 2006
 * <p>
 * @version 1.0
 * @author Matei Costescu
 * @date April 21, 2006
 * <p>
 * @version 1.1
 * @author Alin Alexandru
 * @date April 28, 2006
 * refactoring - use HashMap instead of HashMap
 * <p> 
 * @version 1.2
 * @author Andrei Tapuchievici
 * @date August 03, 2006
 * Added methods to handle default order for user(profile entity)
 * <p>
 * @version 1.3
 * @author Matei Costescu
 * @date January 09, 2007
 * Added getNextId(), addUser(User, Boolean).
 * <p>
 * @version 1.4
 * @author Andrei Tapuchievici
 * @date Feb 20, 2007
 * Added admin right for users
 * <p>
 * @version 1.5
 * @author Andrei Tapuchievici
 * @date March 01, 2007
 * added offset and offset byt percent for default orders
 * <p>
 * @version 1.6
 * @author Istvan-Ferenc Erdely
 * @date April 2007
 * added suordinates to users
 * <p>
 * @version 1.7
 * @author Matei Costescu
 * @date October 30, 2007
 * Added 'quantity_capital' column in 'default_order' table.
 * <p>
 * @version 1.8
 * @author Andrei Tapuchievici
 * @date Dec 05, 2007
 * Added 'TIM', VWAP_S, VWAP_E, VWAP_P, SSA, LSA field and clone() method.
 * <p>
 * Added methods to handle user profile.
 * @version 1.9
 * @author Matei Costescu
 * @date April 11, 2008
 * <p>
 * Added methods for UserRoute.
 * @version 1.10
 * @author Matei Costescu
 * @date June 10, 2008
 * <p>
 * Add user enable feature.
 * @version 1.11
 * @author Erimescu Andra
 * @date October 23, 2008
 * <p> Added new members in DB: is_invisible, post_only, allow_routing, route_to_NYSE,
 * good_after_date,good_till_date.
 * @author Andra Erimescu
 * @version 1.12
 * @date August 17, 2009
 */
public class LocalUsersDBContext extends UsersDBContext {
	
	
	/**
	 * Hardcoded value for Yes
	 */
	private static String YES_VALUE = "YES";

	/**
	 * Hardcoded value for NO
	 */
	private static String NO_VALUE = "NO";
	
    
	//private static final String USER_ACCOUNTS_TABLE_NAME = "user_accounts";
	private static final String DEFAULT_ORDER = "default_order";//v1.2    
    private static final String USER_ADMIN_RIGHT = "user_admin_right";//v1.4
    
    private static final String BROKER_ACCOUNT_TABLE_NAME = "clearing_broker_account";
    private static final String EXECUTING_ACCOUNT_TABLE_NAME = "executing_broker_account";
    private static final String EXECUTING_CONNECTION_TABLE_NAME = "executing_broker_connection";
    private static final String BROKER_ACCOUNT_ID = "clearing_broker_account_id";
    private static final String EXECUTING_ACCOUNT_ID = "executing_broker_account_id";
    private static final String EXECUTING_CONNECTION_ID = "executing_broker_connection_id";
    private static final String ALLOCATION_PROFILE_ID = "allocation_profile_id";
    
    // system_settings
    private final String SYSTEM_SETTINGS = "system_settings";
	private final String SYSTEM_KEY = "system_key";
	private final String SYSTEM_VALUE = "system_value";
	private final String IGNORE_LICENSE_ERRORS = "ignore_license_errors";
	
    //users table
	private static final String NAME = "name"; 
	private static final String IS_DELETED = "is_deleted"; 
  	private static final String IS_ENABLED = "is_enabled";
 	private static final String PASSWORD = "password"; 
	private static final String ROLES= "roles";
 
    //users_accounts table
	private static final String USER_ID = "user_id"; // also used in user_fuction_rights and user_admin_right
    
	private static final String SYMBOL_TYPE_ID = "symbol_type_id";
    
    //default order table columns
    //NOTE: USER_ID is same with the one from USERS_ACCOUNTS table
	private static final String EXCHANGE_GROUP_ID = "exchange_group_id";//v1.2
    /** store broker name*/
	private static final String BROKER_NAME = "clearing_broker_name";//v1.2

	/** store exchange name considered as destination*/
	private static final String DESTINATION = "destination";//v1.2
    
	private static final String QUANTITY = "quantity";//v1.2
	private static final String QUANTITY_CAPITAL = "quantity_capital";//v1.7
	private static final String PRICE = "price";//v1.2
	private static final String DISPLAY_SIZE = "display_size";//v1.2
	private static final String TIF = "tif";//v1.2
	private static final String AUX_PRICE = "aux_price";//v1.2
	private static final String SCALED_GROUP = "scaled_group";//v1.2
	private static final String ORDER_TYPE = "order_type";//v1.2
	private static final String DEFAULT_ORDER_TYPE = "default_order_type";//v1.2
	private static final String OFFSET_VALUE = "offset_value";//v1.5
	private static final String OFFSET_BY_PERCENT = "offset_by_percent";//v1.5
    
	private static final String ADMIN_RIGHT = "admin_right";//v1.4
	private static final String RIGHTS = "rights";//v1.4
	
    private static final String AUX_PRICE_OFFSET = "aux_price_offset";
    private static final String AUX_PRICE_OFFSET_BY_PERCENT = "aux_price_offset_by_percent";
    
    private static final String STRATEGY_TAGS = "strategy_tags";
    
    /**Constant used to represent true boolean value.*/
    private static final String YES = "YES";
    /**Constant used to represent false boolean value.*/
    private static final String NO = "NO";


    private Logger logger = AppLogger.getLogger(AppLogger.LOGGER_NAME_MANAGEMENT_SERVER); 
    
    /**
     * Returns all Users from database. 
     * @since 1.0
     * @return a Vector of User Objects.
     * @see User
     */
    public Vector<User> getUsers() {
//        logger.info("UsersDBContext.getUsers start");
        
        Vector<User> users = new Vector<User>();
        try {
            
            Vector<HashMap> test = dbContext.getTableRows(ManagementConstants.USER_TABLE_NAME);
            for (HashMap h : test) {
                User user = Hash2User(h); //get user
                if (user != null && !user.isDeleted()){ 
                	users.add(user);
				}
            }
            
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
        
        logger.fine("return = "+ users);
        //logger.info("UsersDBContext.getUsers stop");
        return users;
    }
    
    public void completeUsers(Vector<User> users){
        if(users == null ){
            return;
        }
        try {
        	User user = null;
        	int sizeU = users.size();
            for(int i = 0 ; i < sizeU; i++){
            	user = users.get(i);
                user.resetAdminRights();
                setAdminRights(user);
                users.set(i, user);
            }
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
    }
    
    /**
     * Returns the user with the given name (is unique).  
     * @since 1.0
     * @param name
     * @return Returns the user with the given id or null if invalid id.
     */
    public User getUser(String name) {
        //logger.info("START");
        
        User user = null;
        
        try {
            String query = "SELECT * from "+ManagementConstants.USER_TABLE_NAME+
            " where "+NAME+"='"+name+"'";
            
            ///////////// mongo equivalent ///////////////////
            DB db = null;
            DBCollection   coll = null;
            BasicDBObject   qry = null;
            DBCursor     result = null;
            
            qry = new BasicDBObject("_id", new BasicDBObject(NAME, name));
            
            coll = db.getCollection(ManagementConstants.USER_TABLE_NAME);
            
            result = coll.find(qry);
            
            Vector<HashMap> rows = dbContext.getRowsFromQuery(query);
            if (rows.size() > 0) {
                user = Hash2User(rows.get(0));
            } 
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
        
        logger.fine("return = "+user);
        //logger.info("STOP");
        return user;
    }

 
    
    /**
     * method used to set a user with all admin rights from database
     * @param user user objectr that should be setted with admin rights
     * @throws DBException
     * @since v1.4
     */
    private void setAdminRights(User user) throws DBException {
//        String query = 
//            "SELECT * FROM "+USER_ADMIN_RIGHT+
//            " where " + USER_ID + "=" + user.getId();
        
        ////////////// mongo equivalent ////////////////////////////
        DB                 db = null;
        DBCollection     coll = null;
        DBCursor       result = null;
        BasicDBObject   where = null;
        BasicDBObject   query = null;
        
        coll = db.getCollection(USER_ADMIN_RIGHT);
        where = new BasicDBObject(USER_ID, user.getId());
        query = new BasicDBObject("_id", where);
        
        result = coll.find(query);
        
        Vector<HashMap> res = dbContext.getRowsFromQuery(query);
        for (HashMap row : res) {
            try {
                String adminRight       = row.get(ADMIN_RIGHT).toString();
                String rights           = row.get(RIGHTS).toString();
                if (rights != null && 
                    rights.equalsIgnoreCase(UserRights.HAS_RIGHT)){
                    user.setAdminRight(adminRight, true);
                } else {
                    user.setAdminRight(adminRight, false);
                }
            } catch (Exception e) {
                logger.severe(AppLogger.buildExceptionString(e));
            }
        }
        
    }
    
    /**
     * method used to update the database with the user admin rights;
     * first delete all old entries and then write new entries
     * @param user user object that should be updated with
     * @throws DBException
     * @since v1.4
     */
    private boolean updateUserAdminRights(User user) throws DBException {
        try{
//           String deleteQuery = 
//                "DELETE FROM " + USER_ADMIN_RIGHT +
//                " WHERE "+ USER_ID + "=" + user.getId();
            
            //////////////// mongo equivalent //////////////////
            DB  db = null;
            DBCollection coll = null;
            BasicDBObject where = null;
            
            coll = db.getCollection(USER_ADMIN_RIGHT);
            where = new BasicDBObject(USER_ID, user.getId());
            coll.remove(where);
            
            
            dbContext.execute(deleteQuery);
            HashMap<String, Boolean> userAdminRights = 
                user.getAllAdminRights();
            if ( userAdminRights == null ) return true;
            HashMap<String, Object> row = new HashMap<String, Object>();
            
            for (String key : userAdminRights.keySet()) {
                Boolean value = userAdminRights.get(key);
                row.put(USER_ID, user.getId());
                row.put(ADMIN_RIGHT, key );
                if (user.hasAdminRight(key)) {
                    row.put(RIGHTS, UserRights.HAS_RIGHT);
                }else{
                    row.put(RIGHTS, UserRights.HAS_NOT_RIGHT);
                }
                dbContext.insertRow(USER_ADMIN_RIGHT, row);
            }

        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        return true;
    }    
    
    
    private User Hash2User(HashMap row) throws ClassCastException {
        User user =  new User();
        user.setId(Integer.valueOf((String) row.get(USER_ID)));
        user.setName((String) row.get(NAME));
        user.setPassword((String) row.get(PASSWORD));
        String deleted = (String) row.get(IS_DELETED);
        String enabled = (String) row.get(IS_ENABLED);
        if (deleted != null && deleted.equalsIgnoreCase(YES)) {
            user.deleteUser();
		}
        if (enabled != null){
        	if(enabled.equalsIgnoreCase(YES)){
        		user.setEnabled(true);
            }
        	else
        		user.setEnabled(false);
        }	
        //SET THE ROLES. To be protected, break it up
        String strRoles = (String)row.get(ROLES); 
        int roles = 0;
        try {
            roles = Integer.parseInt(strRoles);
        } catch (Exception e) {
        	logger.severe(AppLogger.buildExceptionString(e));
            roles = 0;
        }        
        user.setAdminitratorRole((roles & UserRole.ROLE_ADMIN) != 0);
        user.setManagerRole((roles & UserRole.ROLE_MANAGER) != 0);
        user.setTraderRole((roles & UserRole.ROLE_TRADER) != 0);
            
        return user;	
    }
    

    
    /**
     * Add a new User in database.
     * If successful then sets the 'id' of the input User
     * @since 1.0
     * @change v1.4 set admin rights for user
     * @param user
     * @return operation result
     */
    public boolean addUser(User user) {
//        logger.info("UsersDBContext.addUser start");
//        logger.info("Param User: "+user);
        
        try {
            startTransaction();
            
            //insert into users table
//            String nextid = dbContext.getNextId(ManagementConstants.USERS_TABLE_NAME);
//            user.setId(nextid);//set the id
            
/// need mongo conv. here            
            HashMap<String, Object> row = new HashMap<String, Object>();
            row.put(USER_ID, user.getId());
            row.put(NAME, user.getName());
            row.put(PASSWORD, user.getPassword());
            row.put(ROLES, new Integer(user.getRoles()));
            row.put(IS_DELETED, NO);
            row.put(IS_ENABLED, YES);
            row.put(ManagementConstants.NAME_PREFIX, PrivilegeUtils.SAME_VALUE);
            dbContext.insertRow(ManagementConstants.USER_TABLE_NAME, row);
            
            ////////////////// mongo equivalent ///////////////////////////
            DB                db = null;
            DBCollection    coll = null;
            BasicDBObject    doc = null;
            
            doc = new BasicDBObject(USER_ID, user.getId().
                             append(new BasicDBObject(NAME, user.getName())).
                             append(new BasicDBObject(PASSWORD, user.getPassword())).
                             append(new BasicDBObject(ROLES, new Integer(user.getRoles()))).
                             append(new BasicDBObject(IS_DELETED, NO)).
                             append(new BasicDBObject(IS_ENABLED, YES)).
                             append(new BasicDBObject(ManagementConstants.NAME_PREFIX, PrivilegeUtils.SAME_VALUE)));
            
            coll = db.getCollection("users");
            coll.insert(doc);
              
            //insert user admi rights
            updateUserAdminRights(user);
            commitTransaction();
            
            
        } catch (Exception e) {
//            user.setId(null);
            rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        
        //logger.info("UsersDBContext.addUser stop");
        return true;
    }
    
    /**
     * Add a new User, with a given id, in database.
     * @since 1.3
     * @param user
     * @param hasId
     * @return operation result
     */
    public boolean addUser(User user, Boolean hasId) {
        if (hasId) {
//            logger.info("UsersDBContext.addUser start");
            logger.fine("Param User: "+user);
            
            try {
                startTransaction();
                
                //insert into users table
                HashMap<String, Object> row = new HashMap<String, Object>();
                row.put(USER_ID, user.getId());
                row.put(NAME, user.getName());
                row.put(PASSWORD, user.getPassword());
                row.put(ROLES, + new Integer(user.getRoles()));
                row.put(IS_DELETED, NO);
                row.put(IS_ENABLED, YES);
                dbContext.insertRow(ManagementConstants.USER_TABLE_NAME, row);
                
                ////////////////// mongo equivalent ///////////////////////////
                DB                db = null;
                DBCollection    coll = null;
                BasicDBObject    doc = null;
            
                doc = new BasicDBObject(USER_ID, user.getId().
                             append(new BasicDBObject(NAME, user.getName())).
                             append(new BasicDBObject(PASSWORD, user.getPassword())).
                             append(new BasicDBObject(ROLES, new Integer(user.getRoles()))).
                             append(new BasicDBObject(IS_DELETED, NO)).
                             append(new BasicDBObject(IS_ENABLED, YES)));
                
                coll = db.getCollection(ManagementConstants.USER_TABLE_NAME);
                coll.insert(doc);
                
                commitTransaction();
                  
            } catch (Exception e) {
                rollbackTransaction();
                logger.severe(AppLogger.buildExceptionString(e));
                return false;
            }
            
            //logger.info("UsersDBContext.addUser stop");
            return true;
        } else {
            return addUser(user);
        }
    }
    
    /**
     * Modify an User in database
     * @since 1.0
     * @change v1.4 add user admin rigths
     * @param user - the User to modify
     * @return operation result
     */
    public boolean modifyUser(User user) {
        //logger.info("UsersDBContext.modifyUser start");
        logger.fine("Param User: "+user);
        
        try {
            startTransaction();
            
            updateUserAdminRights(user);//v1.4
            
            //update the users table
//            String query =
//                "UPDATE "+ManagementConstants.USER_TABLE_NAME+
//                " set " + NAME + "='" + user.getName() + "'" + ", " +
//                PASSWORD + "='" + user.getPassword() + "'" + ", " +
//                IS_DELETED + "='" + (user.isDeleted() ? YES : NO) + "'" + ", " +
//                IS_ENABLED + "='" + (user.isEnabled() ? YES : NO) + "'" + ", " +
//                ROLES + "=" + user.getRoles() + "" +
//                " WHERE " + USER_ID + "=" + user.getId();
            
            /////////////// mongo equivalent /////////////////////////////////////
            DB  db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            BasicDBObject   doc = null;
            BasicDBObject   rpl = null;
            
            doc = new BasicDBObject(NAME, user.getName()).append(PASSWORD, user.getPassword()).
                             append(IS_DELETED, (user.isDeleted() ? YES : NO)).
                             append(IS_ENABLED, (user.isEnabled() ? YES : NO)).
                             append(ROLES, user.getRoles());
            
            where = new BasicDBObject(USER_ID, user.getId());
            rpl   = new BasicDBObject("$set", doc);
            
            
            coll = db.getCollection(ManagementConstants.USER_TABLE_NAME);
            coll.update(rpl, where);
            
            dbContext.execute(query.toString()); 
            commitTransaction();
//            //test save begin
//            ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
//            ObjectOutput out = new ObjectOutputStream(bos);
//            out.writeObject(user);
//            out.close();
//            byte[] userDB = bos.toByteArray();
//            saveUserProfile(user, user.getName(), userDB);
//            //saveUserProfile1(user, userDB);
//            //test save end
//            
//            //test get begin
//            //Vector<HashMap> userProfiles = getUserProfile(user);
//            byte[] userFromDB = getUserProfile(user).values().iterator().next();
//            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(userFromDB));
//            User retrievedUser  = (User)in.readObject();
            //test get end
            
        } catch (Exception e) {
            rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        
        //logger.info("UsersDBContext.modifyUser stop");
        return true;
    }
    
    @Override
    public boolean saveUserDefault(User user, byte[] userDefault){
    	try {
    		startTransaction();
//       	String deleteQuery = "DELETE FROM user_default WHERE user_id = " + 
//        		user.getId();
         
         ///////////////////////// mongo equivalent ///////////////////////////
         DB               db = null;
         DBCollection   coll = null;
         BasicDBObject where = null;
         
         where = new BasicDBObject(user_id, user.getId());
         
         coll = db.getCollection(user_default);
         coll.remove(where);
         
        	dbContext.execute(deleteQuery);
    		HashMap<String, Object> defaultData = new HashMap<String, Object>();
    		defaultData.put("user_id", user.getId());
    		defaultData.put("user_default", userDefault);
    		dbContext.insertRowWithBlobs("user_default", defaultData);
    		commitTransaction();
    	} catch (Exception e) {
    		rollbackTransaction();
    		logger.severe(AppLogger.buildExceptionString(e));
    		return false;
    	}
    	return true;
    }
        
    @Override
	public boolean deleteUserDefault(User user) {
    	try {
    		startTransaction();
        	String deleteQuery = "DELETE FROM user_default WHERE user_id = " + 
        		user.getId();
            ///////////////////////// mongo equivalent ///////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            
            coll  = db.getCollection(user.user_default);
            where = new BasicDBObject(user.user_id, user.getId());
            
            coll.remove(where);
            
        	dbContext.execute(deleteQuery);
    		commitTransaction();
    	} catch (Exception e) {
    		rollbackTransaction();
    		logger.severe(AppLogger.buildExceptionString(e));
    		return false;
    	}
		return true;
	}

	public boolean saveUserProfile(User user, String profileName, byte[] userProfile)
    						throws DuplicateNameException {
    	try {
            startTransaction();
            
            HashMap<String, Object> profileData = new HashMap<String, Object>();
            profileData.put("user_id", user.getId());
            profileData.put("profile_name", profileName);
            profileData.put("user_profile", userProfile);
            dbContext.insertRowWithBlobs("user_profile", profileData);
            
            /////////////// mongo equivalent ///////////////////////////
            DBCollection coll = null;
            DB             db = null;
            BasicDBObject doc = null;
            
            coll = db.getCollection("user_profile");
            doc = new BasicDBObject("user_id", user.getId()).
                             append("profile_name", profileName).
                             append("user_profile", userProfile);
            
            coll.insert(doc);

            commitTransaction();
        } catch (Exception e) {
        	rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
    	return true;
    }


	/**
	 * Delete the profiles specified for the User.
	 * @param user - The User for who the the profiles are added.
	 * @param profileNames - The names of the profiles to delete.
	 * 
	 * @since 1.9
	 * @return operation result.
	 */
	public boolean deleteUserProfiles(User user, 
			ArrayList <String> profileNames) {
    	try {
            startTransaction();
            DBUtilsInterface utils = DBManager.getInstance().getDBUtils();
            for (String profileName : profileNames) {						
//            	String deleteQuery = "DELETE FROM user_profile WHERE user_id = " + 
//            	user.getId() + " AND profile_name='" + utils.escape(profileName) + "'";
//               
               ////////////////// mongo equivalent //////////////////////////////
               DB  db = null;
               DBCollection coll = null;
               BasicDBObject where = null;
               
               coll = db.getCollection("user_profile");
               
               where = new BasicDBObject("user_id", user.getId()).
                                  append("profile_name", utils.escape(profileName));
               
               coll.remove(where);
               
            	dbContext.execute(deleteQuery);
                logger.fine("Delete user profile with profile name="+profileName);            	
            }
            commitTransaction();
        } catch (Exception e) {
        	rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
    	return true;

	}

	/**
	 * Modifies given profile.
	 * @param loginContext - The login context.
	 * @param user - The User for who the the profile is added.
	 * @param oldProfileName - The name of the old profile.
	 * @param newProfileName - The name of the new profile.
	 * @param userProfile - The content of the profile.
	 * 
	 * @return operation result.
	 * 
	 * @see User
	 * @since 1.9
	 */
	public Boolean modifyUserProfile(User user, String oldProfileName, 
			String newProfileName, byte[] userProfile) 
				throws DuplicateNameException {
    	try {
            startTransaction();
            DBUtilsInterface utils = DBManager.getInstance().getDBUtils();
//            String deleteQuery = "DELETE FROM user_profile WHERE user_id = " + 
//            	user.getId() + " AND profile_name='" + utils.escape(oldProfileName) + "'";
//            
            ////////////////////// mongo equivalent //////////////////////////////
            DB  db = null;
            DBCollection coll = null;
            BasicDBObject where = null;
            
            coll = db.getCollection(user_profile);
            
            where = new BasicDBObject(user_id, user.getId()).
                               append(profile_name, utils.escape(oldProfileName));
            
            coll.remove(where);
            
            dbContext.execute(deleteQuery);
            HashMap<String, Object> profileData = new HashMap<String, Object>();
            profileData.put("user_id", user.getId());
            profileData.put("profile_name", newProfileName);
            profileData.put("user_profile", userProfile);
            dbContext.insertRowWithBlobs("user_profile", profileData);
            commitTransaction();
        } catch (Exception e) {
        	rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
    	return true;
	}
	
	/**
	 * Provide the profile for the specified User and profile name.
	 * 
	 * @param loginContext login session.
	 * @param user the for who the the profiles are provided.
	 * @param profileName - The name of the profile.
	 * @return the profile or Null if the profile does not exist.
	 * 
	 * @see User
	 * @since 1.9
	 */
	public byte[] getUserProfile(User user, String profileName) {
		try {
			DBUtilsInterface utils = DBManager.getInstance().getDBUtils();
//			final String query = "SELECT * FROM user_profile WHERE user_id = "
//				+ c + " AND profile_name = '" + utils.escape(profileName) + "'";
//         
         ////////////////// mongo equivalent ///////////////////////////
         DB               db = null;
         DBCursor     result = null;
         DBCollection   coll = null;
         BasicDBObject where = null;
         BasicDBObject query = null;
         
         coll = db.getCollection("user_profile");
         
         where = new BasicDBObject("user_id", "c").append("profile_name", utils.escape(profileName));
         query = new BasicDBObject("_id", where);
         
         result = coll.find(query);
         
         
			Vector<HashMap> userProfiles = dbContext.getRowsFromQuerysWithBlob(
					"user_profile", query);
			if (userProfiles.size() == 1) {	
				return (byte[]) userProfiles.get(0).get("user_profile");
			}
		} catch (Exception e) {
			logger.severe(AppLogger.buildExceptionString(e));
		}
		return null;
	}
	
    public Hashtable<String, byte[]> getUserProfiles(User user){
    	Hashtable<String, byte[]> result = new Hashtable<String, byte[]>();
    	try {
    		//return dbContext.getBlob("user_profile", "user_profile", " user_id = '"+user.getId()+"'");
    		Vector<HashMap> userProfiles = dbContext.getRowsFromQuerysWithBlob(
                "user_profile", "SELECT * from user_profile WHERE user_id = "+user.getId());
            
            /////////////////// mongo equivalent ///////////////////////////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            DBCursor        crs = null;
            
            where = new BasicDBObject("user_id", user.getId());
            crs = coll.find(new BasicDBObject("_id", where));
            
            /// not sure how to convert this without some context guidance -BR
    		for (HashMap dbRecord : userProfiles){
    			String profileName = (String) dbRecord.get("profile_name");
    			byte[] profileData = (byte[]) dbRecord.get("user_profile");
    			result.put(profileName, profileData);
    		}
    	} catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
    	return result;
    }

    
    public byte[] getUserDefault(User user){
    	Hashtable<String, byte[]> result = new Hashtable<String, byte[]>();
    	byte[] userDefault = null;
    	try {
    		Vector<HashMap> userProfiles = dbContext.getRowsFromQuerysWithBlob(
    				"user_default", "SELECT * from user_default WHERE user_id = " + user.getId());
            
            ////////////// mongo equivalent //////////////////////////////////
            DB db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            DBCursor        crs = null;
            
            where = new BasicDBObject("user_id", user.getId());
            
            crs = coll.find(new BasicDBObject("_id", where));
            
    		for (HashMap dbRecord : userProfiles){
    			if ( dbRecord.get("user_default") != null){
    				userDefault = (byte[]) dbRecord.get("user_default");
    			}
    		}
    	} catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
    	return userDefault;
    }
	/**
	 * Provide the list of profile names for the specified user.
	 * 
	 * @param user the for who the the profiles are provided.
	 * @return profile names
	 * 
	 * @see User
	 * @since 1.9
	 */
	public ArrayList<String> getUserProfileNames(User user) {
		ArrayList<String> result = new ArrayList<String>();
    	try {
//    		final String query = "SELECT * from user_profile WHERE user_id = "
//    			+ user.getId();
         
            ///////////////////// mongo equivalent ////////////////////////////
            DB               db = null;
            DBCursor       rslt = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            BasicDBObject   qry = null;

            coll = db.getCollection("user_profile");

            where = new BasicDBObject("user_id", user.getId());
            qry = new BasicDBObject("_id", where);

            rslt = coll.find(qry);
         
         
    		Vector<HashMap> userProfiles = dbContext.getRowsFromQuerysWithBlob(
    				"user_profile", query);
    		for (HashMap dbRecord : userProfiles){
    			String profileName = (String) dbRecord.get("profile_name");
    			result.add(profileName);
    		}
    	} catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
    	return result;
	}
    
    /**
     * Removes an User from database
     * @since 1.0
     * @change v1.4 delete admin rights
     * @param user - the User to remove
     * @return operation result
     */
    public boolean deleteUser(User user) {
        //logger.info("UsersDBContext.deleteUser start");
        logger.fine("Param User: "+user);
        
        try {
            startTransaction();
            deleteAdminRigths(user);//v1.4
            
//            String query =
//                "UPDATE "+ManagementConstants.USER_TABLE_NAME+
//                " set " + IS_DELETED+"='"+YES+"', " +
//                ManagementConstants.NAME_PREFIX + "=" +
//				PrivilegeUtils.getUniqSequence() +
//                " where " + USER_ID + "=" + user.getId(); 
            
            ////////////////////////////// mongo equivalent /////////////////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            BasicDBObject   qry = null;
            BasicDBObject   doc = null;
         
            
            coll  = db.getCollection(ManagementConstants.USER_TABLE_NAME);
            where = new BasicDBObject(USER_ID, user.getId());
            doc   = new BasicDBObject(IS_DELETED, YES).
                               append(ManagementConstants.NAME_PREFIX, PrivilegeUtils.getUniqSequence());
            
            qry = new BasicDBObject("$set", doc);
            
            coll.update(qry, where);
            
            dbContext.execute(query);
            commitTransaction();
            
        } catch (Exception e) {
            rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        
        //logger.info("UsersDBContext.deleteUser stop");
        return true;
    }
    
    /**
     * delete admin rights
     * @since 1.4
     * @param user
     * @throws DBException
     */
    private void deleteAdminRigths(User user) throws DBException {
        //logger.info("UsersDBContext.deleteAdminRights start");
//        String query = 
//            "DELETE FROM "+USER_ADMIN_RIGHT+  
//            " WHERE " + USER_ID + "=" + user.getId(); 
        
        ///////////////////// mongo equivalent /////////////////////////
        DB               db = null;
        DBCollection   coll = null;
        BasicDBObject   qry = null;
        BasicDBObject where = null;
        
        coll = db.getCollection(USER_ADMIN_RIGHT);
        
        qry = new BasicDBObject("$exists", "_id");
        where = new BasicDBObject(USER_ID, user.getId());
        
        coll.update(qry, where);
        
        
        dbContext.execute(query);           
        //logger.info("UsersDBContext.deleteAdminRights stop");
    }

    
    /**
     * Removes ALL Users from database except the one in the 
     * loginContext passed.
     * @return operation result
     */
    public boolean deleteUsers(LoginContext loginContext) {
        if(loginContext == null 
                || loginContext.getUser() == null){
            return false;
        }
        
        int userId = loginContext.getUser().getId();
        
        logger.fine("UsersDBContext.deleteUsers start");
        
        try {
            startTransaction();
            
//            String query = 
//
//                "DELETE FROM " + USER_ADMIN_RIGHT 
//                + " WHERE " + USER_ID + " <> " + userId + ";" +
//                " DELETE FROM " + ManagementConstants.USER_SUBORDINATE_USER_TABLE_NAME + ";" +  
//                " DELETE FROM " + ManagementConstants.USER_SUBORDINATE_GROUP_TABLE_NAME;				//2.then delete from users
         
            ///////////////////// mongo equivalent /////////////////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject   qry = null;
            BasicDBObject  test = null;
            BasicDBObject where = null;
            BasicDBObject    rm = null;
            BasicDBObject   rpl = null;
            BasicDBObject   set = null;
            
            coll = db.getCollection(USER_ADMIN_RIGHT);
                   
            qry = new BasicDBObject(USER_ID, new BasicDBObject("$ne", userId)).
                               append("$exists", "_id");
            
            coll.remove(qry);
            
            // now the other two deletes
            coll  = db.getCollection(ManagementConstants.USER_SUBORDINATE_USER_TABLE_NAME);
            where = new BasicDBObject("$exists", "_id");
            coll.remove(where);
            
            coll  = db.getCollection(ManagementConstants.USER_SUBORDINATE_GROUP_TABLE_NAME);
            where = new BasicDBObject("$exists", "_id");
            coll.remove(where);
                     
            dbContext.execute(query.toString());
//            String query2 =
//                "UPDATE "+ManagementConstants.USER_TABLE_NAME+
//                " SET " + IS_DELETED + "='" + YES + "'" +
//                " WHERE " + USER_ID + " <> " + userId + 				//2.then delete from users
//            		" AND " + IS_DELETED + "='" + NO + "'";
            
            //////////////////////// mongo equivalent /////////////////////////
            coll  = db.getCollection(ManagementConstants.USER_TABLE_NAME);
            rpl   = new BasicDBObject(IS_DELETED, YES);
            set   = new BasicDBObject("$set", rpl);       
            where = new BasicDBObject(USER_ID, new BasicDBObject("$ne", userId)).append(IS_DELETED, NO);
            
            coll.update(set, where);
            
            dbContext.execute(query2);
            commitTransaction();
            
        } catch (Exception e) {
            rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        
        logger.fine("UsersDBContext.deleteUsers stop");
        return true;
    }
    
    /**
     * get the default orders for user
     * @since 1.2
     * @param user id
     * @return list of default orders for user from parameters
     * @see User
     */
    public Vector<UserDefaultOrder> getUserSpecificAllDefaultOrders(String userId, 
            String defaultOrderType){
        logger.fine("UsersDBContext.getUsers start");
        
        Vector<UserDefaultOrder> defaultOrders = new Vector<UserDefaultOrder>();
        try {
//            String query = 
//            "SELECT " + DEFAULT_ORDER + ".*, " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_NAME 
//                      + ", " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID +
//            " FROM " + DEFAULT_ORDER + " LEFT OUTER JOIN " + BROKER_ACCOUNT_TABLE_NAME + 
//            " ON " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID + "=" + DEFAULT_ORDER 
//                   + "." + BROKER_ACCOUNT_ID +           
//            " AND " + DEFAULT_ORDER + "." + USER_ID + "=" + userId +
//            " AND " + DEFAULT_ORDER + "." + DEFAULT_ORDER_TYPE + "='" + defaultOrderType + "'";
            
            //////////////////////////// mongo equivalent /////////////////////////////
            DB                db = null;
            DBCollection   coll1 = null;
            DBCollection   coll2 = null;
            DBCollection  tcoll1 = null;
            DBCollection  tcoll2 = null;
            BasicDBObject    qry = null;
            BasicDBObject   test = null;
            BasicDBObject  onqry = null;
            DBCursor        crs1 = null;
            DBCursor        crs2 = null;
            
            coll1 = db.getCollection(DEFAULT_ORDER);
            coll2 = db.getCollection(BROKER_ACCOUNT_TABLE_NAME);
            
            onqry = new BasicDBObject();
            
            // working from the end of the query back...
            qry = new BasicDBObject(DEFAULT_ORDER_TYPE, defaultOrderType);
            crs1 = coll1.find(qry);
            
            qry = new BasicDBObject(USER_ID, userId);
            crs2 = coll1.find(qry);
            
            // need to make a temp collection with fields for BROKER_ACCOUNT_TABLE_NAME.BROKER_ACCOUNT_ID
            // (b_broker_account_id)  and DEFAULT_ORDER.BROKER_ACCOUNT_ID (d_broker_account_id) 
            
            // BROKER_ACCOUNT_TABLE_NAME.BROKER_ACCOUNT_ID first:
            qry = new BasicDBObject("$exists", BROKER_ACCOUNT_ID);
            crs1 = coll2.find(qry);
            
            // pull results from that query (crs1) and insert into temp collection
            while(crs1.hasNext()){
               //rename the key?
               tcoll1.insert(crs1.next());
            }
            
            // now do the same for  DEFAULT_ORDER.BROKER_ACCOUNT_ID 
            qry.clear();
            qry = new BasicDBObject("$exists", BROKER_ACCOUNT_ID);
            crs2 = coll1.find(qry);
            
            // pull results from that query (crs1) and insert into temp collection
            while(crs2.hasNext()){
               tcoll1.insert(crs2.next());
            }
            
            // so now we have b_broker_account_id and d_broker_account_id
            
            Vector<HashMap> dbDefaultOrders = dbContext.getRowsFromQuery(query);
            for (HashMap hashMap : dbDefaultOrders) {
                UserDefaultOrder userDefaultOrder = hash2UserDefaultOrder(hashMap);
                defaultOrders.add(userDefaultOrder);
            }
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
        return defaultOrders;
    }

    /**
     * load the default orders
     * @since 1.2
     * @return list of default orders
     * @see User
     */
    public void loadUserSpecificAllDefaultOrders(Vector<UserDefaultOrder> defaultOrders){
        logger.fine("UsersDBContext.getUsers start");
//        Vector<UserDefaultOrder> defaultOrders = new Vector<UserDefaultOrder>();
        try {
           
// !!!!!!!!!!!!!!!!!! JOIN OPERATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
            String query = 
                "SELECT " + DEFAULT_ORDER + ".*, " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_NAME + ", " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID +                
                " FROM " + DEFAULT_ORDER + " LEFT OUTER JOIN " + BROKER_ACCOUNT_TABLE_NAME +
                " ON " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID + "=" + DEFAULT_ORDER + "." + BROKER_ACCOUNT_ID;
            
            Vector<HashMap> dbDefaultOrders = dbContext.getRowsFromQuery(query);
            for (HashMap hashMap : dbDefaultOrders) {
                UserDefaultOrder userDefaultOrder = hash2UserDefaultOrder(hashMap);
                defaultOrders.add(userDefaultOrder);
            }
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
//        return defaultOrders;
    }
    
    
    /**
     * build object with values from hashmap
     * @since 1.2
     * @param row
     * @return object
     */
    private UserDefaultOrder hash2UserDefaultOrder(HashMap row) throws ClassCastException {
        UserDefaultOrder userDefaultOrder = new UserDefaultOrder();
        userDefaultOrder.setUserId(Integer.parseInt(row.get(USER_ID).toString()));
        userDefaultOrder.setExchangeGroupId(row.get(EXCHANGE_GROUP_ID).toString());
        userDefaultOrder.setSymbolType(SymbolType.getSymbolType(Integer.parseInt(row.get(SYMBOL_TYPE_ID).toString())));
        if (!row.get(BROKER_ACCOUNT_ID).toString().isEmpty()) {
        	userDefaultOrder.setClearingAccountId(Long.parseLong(row.get(BROKER_ACCOUNT_ID).toString()));
        }
        userDefaultOrder.setBrokerName(row.get(BROKER_NAME).toString());
        if (!row.get(EXECUTING_ACCOUNT_ID).toString().isEmpty()) {
        	userDefaultOrder.setExecutingAccountId(Long.parseLong(row.get(EXECUTING_ACCOUNT_ID).toString()));
        }
        if (!row.get(EXECUTING_CONNECTION_ID).toString().isEmpty()) {
        	userDefaultOrder.setExecutingConnectionId(Long.parseLong(row.get(EXECUTING_CONNECTION_ID).toString()));
        }
		Object allocationProfileID = row.get(ALLOCATION_PROFILE_ID);
		if (allocationProfileID != null && !allocationProfileID.toString().isEmpty()) {
			userDefaultOrder.setAllocationModificationRootProfileId(allocationProfileID.toString());
		}
        userDefaultOrder.setDestination(row.get(DESTINATION).toString());
        userDefaultOrder.setQuantity(row.get(QUANTITY).toString());
        userDefaultOrder.setQuantity_Capital(row.get(QUANTITY_CAPITAL).toString()); //v1.7
        userDefaultOrder.setPrice(row.get(PRICE).toString());
        userDefaultOrder.setDisplaySize(row.get(DISPLAY_SIZE).toString());
        userDefaultOrder.setTif(OrderTif.getOrderTif(row.get(TIF).toString()));
        userDefaultOrder.setAuxPrice(row.get(AUX_PRICE).toString());        
        userDefaultOrder.setScaledGroupId(row.get(SCALED_GROUP).toString());
        userDefaultOrder.setOrderType(OrderType.getOrderType(row.get(ORDER_TYPE).toString()));
        userDefaultOrder.setDefaultOrderType(
                row.get(DEFAULT_ORDER_TYPE).toString());
        userDefaultOrder.setOffset( row.get(OFFSET_VALUE).toString());
        userDefaultOrder.setOffsetByPercent(
                row.get(OFFSET_BY_PERCENT).toString());      
        userDefaultOrder.setAuxPriceOffset(
        		row.get(AUX_PRICE_OFFSET).toString());
        userDefaultOrder.setAuxPriceOffsetByPercent(
        		row.get(AUX_PRICE_OFFSET_BY_PERCENT).toString());
		userDefaultOrder.setStrategyTagIds(StrategyTagsSerDes.deserializeStrategyTagIds(row.get(STRATEGY_TAGS).toString()));
        return userDefaultOrder;    
    }
        
    /**
     * get the default orders for user and exchange id
     * @since 1.2
     * @param user id
     * @param exchange Id
     * @return list of default orders for user and exchange from parameters
     * @see User
     */
    public UserDefaultOrder getUserSpecificDefaultOrders(String userId, 
            String exchangeId, String defaultOrderType){
        logger.fine("UsersDBContext.getUsers start");
        
        Vector<UserDefaultOrder> defaultOrders = new Vector<UserDefaultOrder>();
        try {
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! JOIN OPERATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
            String query = 
            "SELECT " + DEFAULT_ORDER + ".*, " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_NAME + ", " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID + 
            " FROM " + DEFAULT_ORDER + " LEFT OUTER JOIN " + BROKER_ACCOUNT_TABLE_NAME +
            " ON " + BROKER_ACCOUNT_TABLE_NAME + "." + BROKER_ACCOUNT_ID + "=" + DEFAULT_ORDER + "." + BROKER_ACCOUNT_ID +
            " AND " + DEFAULT_ORDER + "." + USER_ID + "=" + userId +
            " AND " + DEFAULT_ORDER + "." + DEFAULT_ORDER_TYPE + "='" + defaultOrderType + "'" +
            " AND " + DEFAULT_ORDER + "." + EXCHANGE_GROUP_ID + "='" + exchangeId + "'";

            Vector<HashMap> dbDefaultOrders = dbContext.getRowsFromQuery(query);
            for (HashMap hashMap : dbDefaultOrders) {
                UserDefaultOrder userDefaultOrder = hash2UserDefaultOrder(dbDefaultOrders.elementAt(0));
                return userDefaultOrder;
            }
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
        return null;
        
    }

    
    /**
     * insert or update the object form parameters; the criteria is considered
     * the pair (userId/exchangeId) from the userDefaultOrder object
     * @since 1.1
     * @param user id
     * @param exchange Id
     * @return true for success
     * @see User
     */
    public boolean modifyUserDefaultOrder(UserDefaultOrder userDefaultOrder){
        logger.fine("modifyUserDefaultOrder start");
        try {
            startTransaction();
            String userId = String.valueOf(userDefaultOrder.getUserId());
            String exchangeGroupId = userDefaultOrder.getExchangeGroupId();
            String defaultOrderType = userDefaultOrder.getDefaultOrderType();
            int symbolTypeId = userDefaultOrder.getSymbolType().ordinal();
            if ( userId != null && exchangeGroupId != null && defaultOrderType != null &&
                 userId.length() > 0 && exchangeGroupId.length() > 0 && defaultOrderType.length() > 0) {
                
                String query = 
                    "DELETE FROM " + DEFAULT_ORDER +       //1.delete
                    " WHERE " + USER_ID + "=" + userId +
                    " AND " + SYMBOL_TYPE_ID  + "=" + symbolTypeId +
                    " AND " + EXCHANGE_GROUP_ID + "='" + exchangeGroupId + "'" +                    
                    " AND " + DEFAULT_ORDER_TYPE + "='" + defaultOrderType + "'";
                
                ///////////////////////// mongo equivalent //////////////////////////
                DB               db = null;
                DBCollection   coll = null;
                BasicDBObject where = null;
                
                where = new BasicDBObject(USER_ID, userId).
                                   append(SYMBOL_TYPE_ID, symbolTypeId).
                                   append(EXCHANGE_GROUP_ID, exchangeGroupId).
                                   append(DEFAULT_ORDER_TYPE, defaultOrderType);
                
                coll = db.getCollection(DEFAULT_ORDER);
                
                coll.remove(where);
                
                
                dbContext.execute(query);
                
                HashMap<String, Object> row = new HashMap<String, Object>();
                ///// need some mongo here /////////////////
                row.put(USER_ID, userDefaultOrder.getUserId());
                row.put(EXCHANGE_GROUP_ID, userDefaultOrder.getExchangeGroupId());
                row.put(SYMBOL_TYPE_ID, userDefaultOrder.getSymbolType().ordinal());
                Long lvalue = userDefaultOrder.getClearingAccountId();
                if (lvalue != null) {
                    row.put(BROKER_ACCOUNT_ID, lvalue.toString());
                }
                lvalue = userDefaultOrder.getExecutingAccountId();
                if (lvalue != null) {
                	row.put(EXECUTING_ACCOUNT_ID, lvalue.toString());
                }
                lvalue = userDefaultOrder.getExecutingConnectionId();
                if (lvalue != null) {
                	row.put(EXECUTING_CONNECTION_ID, lvalue.toString());
                }
                String value = userDefaultOrder.getDestinationId();
                if (value != null && value.length() > 0){
                    row.put(DESTINATION, value);
                }
                value = userDefaultOrder.getAllocationProfileModificationRootId();
                if (value != null && value.length() > 0) {
                	row.put(ALLOCATION_PROFILE_ID, value);
                }
                value = userDefaultOrder.getQuantity();
                if (value != null && value.length() > 0){
                    row.put(QUANTITY, value);
                }
                //v1.7 begin
                value = userDefaultOrder.getQuantity_Capital();
                if (value != null && value.length() > 0){
                    row.put(QUANTITY_CAPITAL, value);
                }                
                //v1.7 end                
                if (userDefaultOrder.getPrice() != null){
                	value = userDefaultOrder.getPrice().toString();
                    row.put(PRICE, value);
                }
                if (userDefaultOrder.getAuxPrice() != null){
                	value = userDefaultOrder.getAuxPrice().toString();
                    row.put(AUX_PRICE, value);
                }
                value = userDefaultOrder.getDisplaySize();
                if (value != null && value.length() >0 ){
                    row.put(DISPLAY_SIZE, value);
                }
                if (userDefaultOrder.getTif() != null){
	                value = userDefaultOrder.getTif().getStringRepresentation();
	                if (value != null && value.length() >0 ){
	                    row.put(TIF, value);
	                }
                }
                value = userDefaultOrder.getScaledGroupId();
                if (value != null && value.length() >0 ){
                    row.put(SCALED_GROUP, value);
                }
                if (userDefaultOrder.getOrderType() != null){
	                value = userDefaultOrder.getOrderType().getStringRepresentation();
	                if (value != null && value.length() >0 ){
	                    row.put(ORDER_TYPE, value);
	                }
                }
                
                value = userDefaultOrder.getDefaultOrderType();
                if (value != null && value.length() > 0) {
					row.put(DEFAULT_ORDER_TYPE, value);
				}

				value = userDefaultOrder.getOffset();
				if (value != null && value.length() > 0) {
					row.put(OFFSET_VALUE, value);
				}
				
				value = userDefaultOrder.getOffsetByPercent();
				if (value != null && value.length() > 0) {
					row.put(OFFSET_BY_PERCENT, value);
				}
				
				value = userDefaultOrder.getAuxPriceOffset();
				if(value != null && value.length() > 0) {
					row.put(AUX_PRICE_OFFSET, value);
				}
				
				value = userDefaultOrder.getAuxPriceOffsetByPercent();
				if(value != null && value.length() > 0) {
					row.put(AUX_PRICE_OFFSET_BY_PERCENT, value);
				}
				
				value = StrategyTagsSerDes.serializeStrategyTagIds(userDefaultOrder.getStrategyTagIds()); 
				if (value != null && value.length() > 0) {
					row.put(STRATEGY_TAGS, value);
				}
 				
				dbContext.insertRow(DEFAULT_ORDER, row);
                
                commitTransaction();
            } else {
            	return false;
            }
            
        } catch (Exception e) {
            rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
        
        logger.fine("modifyUserDefaultOrder stop");
        
        return true;        
    }
    
    /**
     * Adds a UserRoute if it doesn't exist or updates an existing one.
     * @since 1.10 
     * @param userRoutes - The UserRoutes.
     * @return boolean - True if the operation is successful, False otherwise.
     */
    public boolean setUserRoutes(ArrayList < UserRoute > userRoutes) {
    	try {
    		startTransaction();
    		int id = -1;
    		for (UserRoute userRoute : userRoutes) {							
    			String deleteQuery = "DELETE FROM user_route WHERE user_route_id = " + userRoute.getId();
                  
                  //////////////// mongo equivalent //////////////////////////////
                  DB               db = null;
                  DBCollection   coll = null;
                  BasicDBObject where = null;
                  
                  coll = db.getCollection("user_route");
                  where = new BasicDBObject("user_route_id", userRoute.getId());
                  coll.remove(where);
                  
    			dbContext.execute(deleteQuery);
    			HashMap<String, Object> userRouteData = new HashMap<String, Object>();            
    			if (userRoute.getId() == -1) {
    				id = Integer.parseInt(dbContext.getNextId("user_route"));
    			} else {
    				id = userRoute.getId();
    			}
    			userRoute.setId(id);
    			userRouteData.put("user_route_id", id);
    			userRouteData.put("user_id", userRoute.getUser().getId());
    			userRouteData.put("broker_name", userRoute.getBrokerName());
    			userRouteData.put("account_name", userRoute.getAccountName());
    			userRouteData.put("exchange_name", userRoute.getExchange().getName());
    			userRouteData.put("destination_name", userRoute.getDestination().getName());
    			dbContext.insertRow("user_route", userRouteData);
    		}
    		commitTransaction();
        } catch (Exception e) {
        	rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
    	return true;
    }
    
    /**
     * Deletes the given UserRoutes.
     * @since 1.10
     * @param userRoutes - The UserRoutes.
     * @return boolean - True if the operation is successful, False otherwise.
     */
    public boolean deleteUserRoutes(ArrayList < UserRoute > userRoutes) {
    	try {
    		startTransaction();
    		for (UserRoute userRoute : userRoutes) {							
    			String deleteQuery = "DELETE FROM user_route WHERE user_route_id = " + userRoute.getId();
                  
                  //////////////// mongo equivalent //////////////////////////////
                  DB               db = null;
                  DBCollection   coll = null;
                  BasicDBObject where = null;
                  
                  coll = db.getCollection("user_route");
                  where = new BasicDBObject("user_route_id", userRoute.getId());
                  coll.remove(where);
                  
                  
    			dbContext.execute(deleteQuery);
    		}
    		commitTransaction();
        } catch (Exception e) {
        	rollbackTransaction();
            logger.severe(AppLogger.buildExceptionString(e));
            return false;
        }
    	return true;
    }
    
    /**
     * @since 1.10
     * @param userId - The id if the User.
     * @return ArrayList < UserRoute > - The list of User routes for the given User.
     */
    public ArrayList < UserRoute > getUserRoutes(User user) {
        ArrayList<UserRoute> userRoutes = new ArrayList<UserRoute>();
        try {
            
            String query = "SELECT * FROM user_route WHERE user_id = " + user.getId();
            
            //////////////// mongo equivalent //////////////////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;

            coll = db.getCollection("user_route");
            where = new BasicDBObject("user_route_id", userRoute.getId());
            coll.find(new BasicDBObject("_id", where));
            
        	Vector<HashMap> rows = dbContext.getRowsFromQuery(query);
            for (HashMap row : rows) {
            	UserRoute userRoute = new UserRoute();
                userRoute.setId(Integer.parseInt((String) row.get("user_route_id")));
                userRoute.setUser(user);
                userRoute.setBrokerName((String) row.get("broker_name"));
                userRoute.setAccountName((String) row.get("account_name"));
                userRoute.setExchange(new Exchange((String) row.get("exchange_name")));
                userRoute.setDestination(new Exchange((String) row.get("destination_name")));
                
            	userRoutes.add(userRoute);
            }
            
        } catch (Exception e) {
            logger.severe(AppLogger.buildExceptionString(e));
        }
        
        return userRoutes;
    }

    /**
     * @since 1.3
     * @return - Next value from the sequence of the table.
     * @throws DBException
     */
    public String getNextId() throws DBException {
        String id = null;
        startTransaction();
        try {
            id = dbContext.getNextId(ManagementConstants.USER_TABLE_NAME);
        } catch (Exception e) {
        	logger.severe(AppLogger.buildExceptionString(e));
            rollbackTransaction();
            return null;
        }
        commitTransaction();
        return id;
    }
    
	public boolean saveIgnoreLicenseErrors(boolean ignoreLicense) throws DBException {
		TransactionalContextInterface tc = null;
		try {
			tc = new TransactionalContext();
			return saveIgnoreLicenseErrors(tc, ignoreLicense);
		} finally {
			if (tc != null) {
				tc.release();
			}
		}
	}
	
	private boolean saveIgnoreLicenseErrors(TransactionalContextInterface tc,
			boolean ignoreLicense) throws DBException {
		boolean success = false;
		String sqlStr = null;
		String ignoreLicenseStr = ignoreLicense ? YES_VALUE : NO_VALUE;
		try {
			// UPDATE
			sqlStr = "DELETE FROM " + SYSTEM_SETTINGS + " WHERE " + SYSTEM_KEY
					+ " = '" + IGNORE_LICENSE_ERRORS + "'";
                  
                  //////////////// mongo equivalent //////////////////////////////
                  DB               db = null;
                  DBCollection   coll = null;
                  BasicDBObject where = null;
			
                  coll = db.getCollection(SYSTEM_SETTINGS);
                  where = new BasicDBObject(SYSTEM_KEY, IGNORE_LICENSE_ERRORS);
                  coll.remove(where);
                  
                  tc.execute(sqlStr);

			sqlStr = "INSERT INTO " + SYSTEM_SETTINGS + " (" + SYSTEM_KEY
					+ ", " + SYSTEM_VALUE + ") VALUES ('"
					+ IGNORE_LICENSE_ERRORS + "', '"
					+ ignoreLicenseStr + "')";
                  
                  where = new BasicDBObject(SYSTEM_KEY, IGNORE_LICENSE_ERRORS).
                                     append(SYSTEM_VALUE, ignoreLicenseStr);
                  
                  coll.insert(where);
                  
			tc.execute(sqlStr);
			success = true;
		} catch (DBException e) {
			logger.severe("Error saving startOfDay timestamp; "
					+ ", exception message: "
					+ AppLogger.buildExceptionString(e));
			throw e;
		}
		return success;
	}
	
	public boolean readIgnoreLicenseErrors() throws DBException {
		boolean ignoreLicense = false;
		String strSelect = "SELECT " + SYSTEM_VALUE + " FROM "
				+ SYSTEM_SETTINGS + " WHERE " + SYSTEM_KEY + " = '"
				+ IGNORE_LICENSE_ERRORS + "'";
            
            /////////////////// mongo equivalent ////////////////////////
            DB               db = null;
            DBCollection   coll = null;
            BasicDBObject where = null;
            DBCursor        crs = null;
            
            coll = db.getCollection(SYSTEM_SETTINGS);
            
            crs = coll.find(new BasicDBObject(SYSTEM_VALUE, where));

		DBContextInterface dbContext = null;
		try {
			dbContext = DBManager.getInstance().getDBContext();
			Vector<HashMap> vHmSettings = dbContext.getRowsFromQuery(strSelect);
			int len = vHmSettings.size();
			if (len != 1) {
				return false;
			}
			HashMap settingsRecord = vHmSettings.elementAt(0);
			String ignoreLicenseStr = StringTemplate.objectToString(
					settingsRecord.get(SYSTEM_VALUE), NO_VALUE);
			if (ignoreLicenseStr.equals(YES_VALUE)) {
				ignoreLicense = true;
			}
		} catch (DBException e) {
			logger.severe("Error Loading start day timestamp: "
					+ AppLogger.buildExceptionString(e));
			throw e;
		} finally {
			DBManager.getInstance().releaseContext(dbContext);
		}
		return ignoreLicense;
	}
}