/**
 * @(#)ScaledGroupDbContext.java
 * @version: 1.7
 * 
 * Copyright (c) 2005-2009 Green Mountain Analytics 
 * This software is the confidential and proprietary information 
 * of Green Mountain Analytics. You shall not disclose such Confidential   
 * Information and shall use it only in accordance with the terms 
 * of the license agreement you entered into with Green Mountain Analytics.
 */

package com.gma.managementserver.local.group.scaled;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import com.gma.dbaccess.DBException;
import com.gma.group.Group;
import com.gma.group.orderticketpattern.AmountType;
import com.gma.group.orderticketpattern.DBOrderTicketPattern;
import com.gma.group.orderticketpattern.IOrderTicketPattern;
import com.gma.group.orderticketpattern.OrderTif;
import com.gma.group.orderticketpattern.OrderType;
import com.gma.group.orderticketpattern.PriceType;
import com.gma.group.scaled.InvalidScaledGroupSpecificationException;
import com.gma.group.scaled.ScaledGroup;
import com.gma.group.scaled.ScaledGroupMember;
import com.gma.group.scaled.ScaledGroupValidator;
import com.gma.log.AppLogger;
import com.gma.managementserver.group.scaled.ScaledGroupDbContext;
import com.gma.managementserver.local.DBContextUtils;
import com.gma.managementserver.local.ManagementConstants;
import com.gma.managementserver.local.PrivilegeUtils;
import com.gma.serialization.common.ObjectToStringSerializer;
import com.gma.strategytag.StrategyTagsSerDes;
import com.gma.trading.ActionType;
import com.gmanalytics.algorithms.values.sets.IDefaultDefinedValueSetReadWrite;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

@SuppressWarnings("rawtypes")
public class LocalScaledGroupDbContext extends ScaledGroupDbContext {

	private static final String OWNER_ID = "owner_id";
	private static final String IS_LOCKED = "is_locked";
	private static final String YES = "YES";
	private static final String NO = "NO";
	private static final String IS_DELETED = "is_deleted";
	private static final String GROUP_NAME = "name";
	private static final String GROUP_ID = "management_group_id";
	private static final String GROUP_TYPE = "group_type";
	private static final String SCOPE = "scope";

	private static final String DETAIL_TABLE_NAME = "scaled_group_detail";

	private static final String COLUMN_ORDER_TICKET_PATTERN_ID = "order_ticket_pattern_id";
	private static final String COLUMN_MEMBER_INDEX = "member_index";
	private static final String COLUMN_MEMBER_GROUP_ID = "group_id";
	
	private static final String ORDER_TICKET_PATTERN_TABLE_NAME = "order_ticket_pattern";
	private static final String COLUMN_OPERATION = "operation";
	private static final String COLUMN_DISPLAY_SIZE = "display_size";
	private static final String COLUMN_CLEARING_BROKER_ACCOUNT_ID = "clearing_broker_account_id";
	private static final String COLUMN_EXECUTING_BROKER_ACCOUNT_ID = "executing_broker_account_id";
	private static final String COLUMN_EXECUTING_BROKER_CONNECTION_ID = "executing_broker_connection_id";
	private static final String COLUMN_EXCHANGE_NAME = "exchange_name";
	private static final String COLUMN_ORDER_TYPE = "order_type";
	private static final String COLUMN_TIF = "tif";
	private static final String COLUMN_AMOUNT_TYPE = "amount_type";
	private static final String COLUMN_AMOUNT_VALUE = "amount_value";
	private static final String COLUMN_ORIGINAL_PRICE_BY_PERCENT = "original_price_by_percent";
	private static final String COLUMN_ORIGINAL_PRICE = "original_price";
	private static final String COLUMN_ORIGINAL_PRICE_OFFSET = "original_price_offset";
	private static final String COLUMN_ORIGINAL_AUX_PRICE_BY_PERCENT = "original_aux_price_by_percent";
	private static final String COLUMN_ORIGINAL_AUX_PRICE = "original_aux_price";
	private static final String COLUMN_ORIGINAL_AUX_PRICE_OFFSET = "original_aux_price_offset";
	private static final String COLUMN_ALGORITHM = "algorithm";
	private static final String COLUMN_PP_ALLOCATION_PROFILE = "pp_allocation_profile_id";
	private static final String COLUMN_ALGORITHM_ATTRIBUTES = "algorithm_attributes";
	private static final String COLUMN_USER_CREATOR_ID = "user_creator_id";
	private static final String COLUMN_STRATEGY_TAGS = "strategy_tags";

	private Logger logger = AppLogger.getLogger(AppLogger.LOGGER_NAME_MANAGEMENT_SERVER);

	/**
	 * 
	 * @see com.gma.managementserver.group.scaled.ScaledGroupDbContext#loadAllGroups(com.gma.managementserver.user_relation.UserRelationManager)
	 */
	public ScaledGroup[] loadAllGroups() {
		List<ScaledGroup> vRes = new ArrayList<ScaledGroup>();
//		String strSelectAllBaskets = "SELECT * FROM " + ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME + " WHERE "
//				+ GROUP_TYPE + "= '" + Group.GROUP_TYPE_ORDER_PROFILE + "'";
      
      ///////////// Mongo equivalent /////////////////////////////////////////////////
      DB db = null;
      BasicDBObject where = new BasicDBObject(GROUP_TYPE, Group.GROUP_TYPE_ORDER_PROFILE);
      BasicDBObject query = new BasicDBObject("_id", where);
      DBCursor     result = null;
      
      DBCollection coll = db.getCollection(ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME);
      
      result = coll.find(query, where);
      
		try {
			Vector<HashMap> vQueryResult = dbContext.getRowsFromQuery(strSelectAllBaskets);
			for (HashMap row : vQueryResult) {
				try {
					Integer ownerId = DBContextUtils.getInteger(row.get(OWNER_ID), logger);
					if (ownerId == null) {
						continue;
					}
					String groupName = DBContextUtils.getString(row.get(GROUP_NAME), logger);
					String groupId = DBContextUtils.getString(row.get(GROUP_ID), logger);
					String isDeleted = DBContextUtils.getString(row.get(IS_DELETED), logger);
					String isLocked = DBContextUtils.getString(row.get(IS_LOCKED), logger);
					ScaledGroup scaledGroup = new ScaledGroup(groupId, groupName, ownerId, YES.equals(isLocked),
							YES.equals(isDeleted));
					ScaledGroupMember[] members = loadMembers(groupId);
					for (ScaledGroupMember scaledGroupMember : members) {
						scaledGroup.addMember(scaledGroupMember);
					}
					vRes.add(scaledGroup);
				} catch (InvalidScaledGroupSpecificationException e) {
					logger.severe(AppLogger.buildExceptionString(e));
				}
			}
		} catch (DBException e) {
			logger.severe(AppLogger.buildExceptionString(e));
		}
		return vRes.toArray(new ScaledGroup[vRes.size()]);
	}

	public boolean deleteGroup(String groupId) {
		try {
			startTransaction();
//			 update scaled group
			HashMap<String, String> groupDetails = new HashMap<String, String>();
			groupDetails.put(IS_DELETED, YES);
			HashMap<String, String> condition = new HashMap<String, String>();
			condition.put(GROUP_ID, groupId);
			dbContext.updateRows(ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME, groupDetails, condition);

			// delete existing items
			HashMap<String, String> itemsDetails = new HashMap<String, String>();
			itemsDetails.put(IS_DELETED, YES);
			HashMap<String, String> itemsCondition = new HashMap<String, String>();
			itemsCondition.put(COLUMN_MEMBER_GROUP_ID, groupId);
			dbContext.updateRows(DETAIL_TABLE_NAME, itemsDetails, itemsCondition);

			commitTransaction();
		} catch (Exception e) {
			rollbackTransaction();
			logger.severe(AppLogger.buildExceptionString(e));
			return false;
		}
		return true;
	}

	public ScaledGroup addGroup(Integer ownerId, String groupName, IOrderTicketPattern[] members) {
		if (groupName == null) {
			logger.severe("Invalid ScaledGroup, name:" + groupName);
			return null;
		}
		if (ownerId == null) {
			logger.severe("Missing owner for ScaledGroup:" + groupName);
			return null;
		}
		List<ScaledGroupMember> scaledGroupMembers = new ArrayList<ScaledGroupMember>();
		String groupId = null;
		try {
			startTransaction();

			groupId = dbContext.getNextId(ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME);
			HashMap<String, String> groupDetails = new HashMap<String, String>();
			groupDetails.put(GROUP_ID, groupId);
			groupDetails.put(GROUP_NAME, groupName);
			groupDetails.put(GROUP_TYPE, "" + Group.GROUP_TYPE_ORDER_PROFILE);
			groupDetails.put(SCOPE, "0");
			groupDetails.put(IS_DELETED, NO);
			groupDetails.put(OWNER_ID, ownerId.toString());
			groupDetails.put(IS_LOCKED, NO);
			groupDetails.put(ManagementConstants.NAME_PREFIX, String.valueOf(PrivilegeUtils.SAME_VALUE));
			dbContext.insertRow(ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME, groupDetails);

			storeDetails(ownerId, members, scaledGroupMembers, groupId);

			commitTransaction();
		} catch (Exception e) {
			rollbackTransaction();
			logger.severe(AppLogger.buildExceptionString(e));
			return null;
		}
		ScaledGroup scaledGroup = new ScaledGroup(groupId, groupName, ownerId, false, false);
		for (ScaledGroupMember scaledGroupMember : scaledGroupMembers) {
			scaledGroup.addMember(scaledGroupMember);
		}
		return scaledGroup;
	}

	public ScaledGroup updateGroup(Integer ownerId, String groupId, String newGroupName, IOrderTicketPattern[] members) {
		if (newGroupName == null) {
			logger.severe("Invalid ScaledGroup, name:" + newGroupName);
			return null;
		}
		if (ownerId == null) {
			logger.severe("Missing owner for ScaledGroup:" + newGroupName);
			return null;
		}
		List<ScaledGroupMember> scaledGroupMembers = new ArrayList<ScaledGroupMember>();
		try {
			startTransaction();
			// update scaled group
			HashMap<String, String> groupDetails = new HashMap<String, String>();
			groupDetails.put(GROUP_NAME, newGroupName);
			HashMap<String, String> condition = new HashMap<String, String>();
			condition.put(GROUP_ID, groupId);
			dbContext.updateRows(ManagementConstants.ORDER_TICKET_GROUP_TABLE_NAME, groupDetails, condition);

			// delete existing items
			HashMap<String, String> itemsDetails = new HashMap<String, String>();
			itemsDetails.put(IS_DELETED, YES);
			HashMap<String, String> itemsCondition = new HashMap<String, String>();
			itemsCondition.put(COLUMN_MEMBER_GROUP_ID, groupId);
			dbContext.updateRows(DETAIL_TABLE_NAME, itemsDetails, itemsCondition);

			// store the new items
			storeDetails(ownerId, members, scaledGroupMembers, groupId);

			commitTransaction();
		} catch (Exception e) {
			rollbackTransaction();
			logger.severe(AppLogger.buildExceptionString(e));
			return null;
		}
		ScaledGroup scaledGroup = new ScaledGroup(groupId, newGroupName, ownerId, false, false);
		for (ScaledGroupMember scaledGroupMember : scaledGroupMembers) {
			scaledGroup.addMember(scaledGroupMember);
		}
		return scaledGroup;
	}

	private void storeDetails(Integer ownerId, IOrderTicketPattern[] members,
			List<ScaledGroupMember> scaledGroupMembers, String groupId) throws DBException, IOException {
		int crtMemberIndex = 0;
		for (IOrderTicketPattern member : members) {
			if (member == null) {
				continue;
			}
			String patternId = dbContext.getNextId(ORDER_TICKET_PATTERN_TABLE_NAME);
			HashMap<String, String> orderPatternDetails = new HashMap<String, String>();
			orderPatternDetails.put(COLUMN_ORDER_TICKET_PATTERN_ID, patternId);
			if (member.getOperation() != null) {
				orderPatternDetails.put(COLUMN_OPERATION, member.getOperation().getText());
			}
			orderPatternDetails.put(COLUMN_DISPLAY_SIZE, member.getDisplaySize());
			if (member.getClearingBrokerAccountId() != null) {
				orderPatternDetails.put(COLUMN_CLEARING_BROKER_ACCOUNT_ID, member.getClearingBrokerAccountId()
						.toString());
			}
			if (member.getExecutingBrokerAccountId() != null) {
				orderPatternDetails.put(COLUMN_EXECUTING_BROKER_ACCOUNT_ID, member.getExecutingBrokerAccountId()
						.toString());
			}
			if (member.getExecutingBrokerConnectionId() != null) {
				orderPatternDetails.put(COLUMN_EXECUTING_BROKER_CONNECTION_ID, member.getExecutingBrokerConnectionId()
						.toString());
			}
			List<Integer> tagIds = member.getStrategyTagIds();
			if (tagIds != null) {
				orderPatternDetails.put(COLUMN_STRATEGY_TAGS, StrategyTagsSerDes.serializeStrategyTagIds(tagIds));
			}		
			if (member.getPPAllocationProfileId() != null) {
				orderPatternDetails.put(COLUMN_PP_ALLOCATION_PROFILE, member.getPPAllocationProfileId()
						.toString());
			}
			orderPatternDetails.put(COLUMN_EXCHANGE_NAME, member.getExchangeId());
			if (member.getOrderType() != null) {
				orderPatternDetails.put(COLUMN_ORDER_TYPE, member.getOrderType().getStringRepresentation());
			}
			if (member.getTif() != null) {
				orderPatternDetails.put(COLUMN_TIF, member.getTif().getStringRepresentation());
			}
			if (member.getAmountType() != null) {
				orderPatternDetails.put(COLUMN_AMOUNT_TYPE, member.getAmountType().toString());
			}
			if (member.getAmountValue() != null) {
				orderPatternDetails.put(COLUMN_AMOUNT_VALUE, member.getAmountValue().toString());
			}
			if (member.getOriginalPriceByPercent() != null) {
				orderPatternDetails
						.put(COLUMN_ORIGINAL_PRICE_BY_PERCENT, member.getOriginalPriceByPercent().toString());
			}
			orderPatternDetails.put(COLUMN_ORIGINAL_PRICE, member.getOriginalPrice());
			if (member.getOriginalPriceOffset() != null) {
				orderPatternDetails.put(COLUMN_ORIGINAL_PRICE_OFFSET, member.getOriginalPriceOffset().toString());
			}
			if (member.getOriginalAuxPriceByPercent() != null) {
				orderPatternDetails.put(COLUMN_ORIGINAL_AUX_PRICE_BY_PERCENT, member.getOriginalAuxPriceByPercent()
						.toString());
			}
			orderPatternDetails.put(COLUMN_ORIGINAL_AUX_PRICE, member.getOriginalAuxPrice());
			if (member.getOriginalAuxPriceOffset() != null) {
				orderPatternDetails
						.put(COLUMN_ORIGINAL_AUX_PRICE_OFFSET, member.getOriginalAuxPriceOffset().toString());
			}
			orderPatternDetails.put(COLUMN_ALGORITHM, member.getAlgorithmName());
			if (member.getAlgorithmAttributes() != null) {
				orderPatternDetails.put(COLUMN_ALGORITHM_ATTRIBUTES,
						ObjectToStringSerializer.writeObjectToString(member.getAlgorithmAttributes()));
			}
			if (member.getUserCreatorId() != null) {
				orderPatternDetails.put(COLUMN_USER_CREATOR_ID, member.getUserCreatorId().toString());
			}
			dbContext.insertRow(ORDER_TICKET_PATTERN_TABLE_NAME, orderPatternDetails);

			HashMap<String, String> scaledGroupMemberDetails = new HashMap<String, String>();
			scaledGroupMemberDetails.put(COLUMN_ORDER_TICKET_PATTERN_ID, patternId);
			scaledGroupMemberDetails.put(COLUMN_MEMBER_GROUP_ID, groupId);
			scaledGroupMemberDetails.put(COLUMN_MEMBER_INDEX, String.valueOf(crtMemberIndex));
			scaledGroupMemberDetails.put(IS_DELETED, NO);
			dbContext.insertRow(DETAIL_TABLE_NAME, scaledGroupMemberDetails);

			String strategyTagIds = StrategyTagsSerDes.serializeStrategyTagIds(member.getStrategyTagIds());
			DBOrderTicketPattern dbOrderTicketPattern = new DBOrderTicketPattern(patternId, ownerId,
					member.getOperation(), member.getDisplaySize(), member.getClearingBrokerAccountId(),
					member.getExecutingBrokerAccountId(), member.getExecutingBrokerConnectionId(),
					member.getExchangeId(), member.getOrderType(), member.getTif(), member.getAmountType(),
					member.getAmountValue(), member.getOriginalPriceByPercent(), member.getOriginalPrice(),
					member.getOriginalPriceOffset(), member.getOriginalAuxPriceByPercent(),
					member.getOriginalAuxPrice(), member.getOriginalAuxPriceOffset(), member.getAlgorithmName(),
					member.getAlgorithmAttributes(), member.getPPAllocationProfileId(), strategyTagIds);
			ScaledGroupMember scaledGroupMember = new ScaledGroupMember(groupId, crtMemberIndex, false,
					dbOrderTicketPattern);
			scaledGroupMembers.add(scaledGroupMember);
			crtMemberIndex++;
		}
	}

	private IOrderTicketPattern getOrderTicketPattern(String orderTicketPatternId) throws DBException,
			ClassNotFoundException, IOException {
		String strSelectGroupDetails = "SELECT * FROM " + ORDER_TICKET_PATTERN_TABLE_NAME + " WHERE "
				+ COLUMN_ORDER_TICKET_PATTERN_ID + "=" + orderTicketPatternId;
		Vector<HashMap> queryResult = dbContext.getRowsFromQuery(strSelectGroupDetails);
		for (HashMap row : queryResult) {
			ActionType operation = DBContextUtils.getActionType(row.get(COLUMN_OPERATION), logger);
			String displaySize = DBContextUtils.getString(row.get(COLUMN_DISPLAY_SIZE), logger);
			Long clearingBrokerAccountId = DBContextUtils.getLong(row.get(COLUMN_CLEARING_BROKER_ACCOUNT_ID), logger);
			Long executingBrokerAccountId = DBContextUtils.getLong(row.get(COLUMN_EXECUTING_BROKER_ACCOUNT_ID), logger);
			Long executingBrokerConnectionId = DBContextUtils.getLong(row.get(COLUMN_EXECUTING_BROKER_CONNECTION_ID),
					logger);
			String tagListStr = DBContextUtils.getString(row.get(COLUMN_STRATEGY_TAGS), logger);
			Long ppAllocationProfileId = DBContextUtils.getLong(row.get(COLUMN_PP_ALLOCATION_PROFILE),
					logger);
			String exchangeId = DBContextUtils.getString(row.get(COLUMN_EXCHANGE_NAME), logger);
			OrderType orderType = DBContextUtils.getOrderType(row.get(COLUMN_ORDER_TYPE), logger);
			OrderTif tif = DBContextUtils.getOrderTif(row.get(COLUMN_TIF), logger);
			AmountType quantityType = DBContextUtils.getAmountType(row.get(COLUMN_AMOUNT_TYPE), logger);
			Double quantityValue = DBContextUtils.getDouble(row.get(COLUMN_AMOUNT_VALUE), logger);
			PriceType originalPriceByPercent = DBContextUtils.getPriceType(row.get(COLUMN_ORIGINAL_PRICE_BY_PERCENT),
					logger);
			String originalPrice = DBContextUtils.getString(row.get(COLUMN_ORIGINAL_PRICE), logger);
			Double originalPriceOffset = DBContextUtils.getDouble(row.get(COLUMN_ORIGINAL_PRICE_OFFSET), logger);
			PriceType originalAuxPriceByPercent = DBContextUtils.getPriceType(
					row.get(COLUMN_ORIGINAL_AUX_PRICE_BY_PERCENT), logger);
			String originalAuxPrice = DBContextUtils.getString(row.get(COLUMN_ORIGINAL_AUX_PRICE), logger);
			Double originalAuxPriceOffset = DBContextUtils.getDouble(row.get(COLUMN_ORIGINAL_AUX_PRICE_OFFSET), logger);
			String algorithm = DBContextUtils.getString(row.get(COLUMN_ALGORITHM), logger);
			IDefaultDefinedValueSetReadWrite attributes = DBContextUtils.getDefinedValueSet(
					row.get(COLUMN_ALGORITHM_ATTRIBUTES), logger);
			Integer userCreatorId = DBContextUtils.getInteger(row.get(COLUMN_USER_CREATOR_ID), logger);
			return new DBOrderTicketPattern(orderTicketPatternId, userCreatorId, operation, displaySize,
					clearingBrokerAccountId, executingBrokerAccountId, executingBrokerConnectionId, exchangeId,
					orderType, tif, quantityType, quantityValue, originalPriceByPercent, originalPrice,
					originalPriceOffset, originalAuxPriceByPercent, originalAuxPrice, originalAuxPriceOffset,
					algorithm, attributes, ppAllocationProfileId, tagListStr);
		}
		return null;
	}

	private ScaledGroupMember[] loadMembers(String groupId) throws InvalidScaledGroupSpecificationException {
		List<ScaledGroupMember> members = new ArrayList<ScaledGroupMember>();
		List<IOrderTicketPattern> patterns = new ArrayList<IOrderTicketPattern>();
		try {
			String strSelectGroupDetails = constructGroupDetailQuery(groupId);
			Vector<HashMap> vQueryResult = dbContext.getRowsFromQuery(strSelectGroupDetails);
			for (HashMap row : vQueryResult) {
				String orderTicketPatternId = DBContextUtils.getString(row.get(COLUMN_ORDER_TICKET_PATTERN_ID), logger);
				Integer memberIndex = DBContextUtils.getInteger(row.get(COLUMN_MEMBER_INDEX), logger);
				IOrderTicketPattern orderTicketPattern = getOrderTicketPattern(orderTicketPatternId);
				patterns.add(orderTicketPattern);
				ScaledGroupMember scaledGroupMember = new ScaledGroupMember(groupId, memberIndex, false,
						orderTicketPattern);
				members.add(scaledGroupMember);
			}
		} catch (Exception e) {
			logger.severe(AppLogger.buildExceptionString(e));
		}

		ScaledGroupValidator.validateScaledGroup((IOrderTicketPattern[]) patterns
				.toArray(new IOrderTicketPattern[patterns.size()]));

		return members.toArray(new ScaledGroupMember[members.size()]);
	}

	private /*String*/ DBCursor constructGroupDetailQuery(String groupId) {
//		return "SELECT * FROM " + DETAIL_TABLE_NAME + " WHERE " + IS_DELETED + " = '" + NO + "' AND group_id="
//				+ groupId + " ORDER BY member_index";
      
      ///////////// Mongo equivalent /////////////////////////////////////////////////
      DB db = null;
      BasicDBObject where = new BasicDBObject(IS_DELETED, NO).append(group_id, groupId);
      BasicDBObject query = new BasicDBObject("_id", where);
      DBCursor     result = null;
      
      DBCollection coll = db.getCollection(DETAIL_TABLE_NAME);
      
      result = coll.find(query, where).sort(new BasicDBObject(member_index, 1));
      
      return result;
	}
}